@section('script')

<!-- Bootstrap tether Core JavaScript -->
<script src="assets/node_modules/popper/popper.min.js"></script>

<!-- DatePicker -->
<script src="assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>

<!-- This is data table -->
<script src="assets/node_modules/datatables/jquery.dataTables.min.js"></script>
<!-- start - This is for export functionality only -->
<script src="assets/node_modules/datatable-button/dataTables.buttons.min.js"></script>
<script src="assets/node_modules/datatable-button/buttons.flash.min.js"></script>
<script src="assets/node_modules/datatable-button/jszip.min.js"></script>
<script src="assets/node_modules/datatable-button/pdfmake.min.js"></script>
<script src="assets/node_modules/datatable-button/vfs_fonts.js"></script>
<script src="assets/node_modules/datatable-button/buttons.html5.min.js"></script>
<script src="assets/node_modules/datatable-button/buttons.print.min.js"></script>

<script>

$( "#boton-enviando" ).click(function() {
  botonEnviar();
});

function botonEnviar(){
  var elem = ["razon_social", "subsector", "actividad", "domicilio_legal", "distrito", "provincia", "persona1", "persona2", "correo1",
              "correo2", "telefono1", "telefono2", "nombre_instalacion", "hora_inicio", "hora_termino", "area_afectada",
              "cantidad_derramada", "unidad_derramada"];
  if(verificarDatos(elem)){
    var formulario = new FormData($("#formulario_registrar_emergencias")[0]);
    formulario.append("emergencias_estado",1);
    
    console.log(formulario);
    $.ajax({
        url: 'api/emergencias',
        data: formulario,
        type: 'post',
        processData: false,
        contentType: false,
        beforeSend: function (a) {
            $(".preloader").show();
        },
        complete: function (a) {
            $(".preloader").hide();
        },
        error: function (data) {

            $(".preloader").hide();
            swal("Error", "Por favor comuniquese con el ADMIN", 'error');

        },
        success: function (salida) {

            if (salida.tipo == 'ok') {

                alerta("Completado", "Se ha registrado correctamente su emergencia.", "success");

            }
            else {
                swal("Error", salida, "warning")
            }
        }
    });
  } else {
    swal("Error", "Por favor, rellene todos los campos", 'error');
  }
}

function verificarDatos(arr){
  for(var i=0; i < arr.length; i++){
    if(document.getElementById(arr[i]).value == "" ){
      swal("Error", "Debe ingresar toda la información necesaria.", 'error');
      $('#'+arr[i]).focus();
      return false;
    }
  }
  return true;
}

</script>

@endsection