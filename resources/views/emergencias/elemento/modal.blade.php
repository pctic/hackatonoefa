<div class="modal fade" id="modal-emergencias">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="box box-primary">
        <div class="modal-header with-border">
          <h3 class="box-title" id="titulo-emergencias">Reporte Preliminar de Emergencias Ambientales</h3>
        </div>
        <div class="modal-body">

          <form id="formulario_registrar_emergencias" type="post" autocomplete="off" action="javascript:void(0);">
            <div class="row">
              <div class="col-12 col-md-6">
                <h4>Datos del Administrado</h4>
                <div class="row">
                  <div class="col-12">
                    <div class="form-group">
                      <label for="razon_social">Nombre o Razón Social</label>
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="basic-addon1"><i class="fas fa-cubes"></i></span>
                        </div>
                        <input type="text" class="form-control" id="razon_social" name="razon_social" >
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12 col-md-4">
                    <div class="form-group">
                      <label for="subsector">Subsector</label>
                      <select name="subsector" id="subsector" class="custom-select">
                        <option value="" selected>Elija Subsector</option>
                        <option value="Electricidad">Electricidad</option>
                        <option value="Hidrocarburos">Hidrocarburos</option>
                        <option value="Industria">Industria</option>
                        <option value="Minería">Minería</option>
                        <option value="Pesquería">Pesquería</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-12 col-md-8">
                    <div class="form-group">
                      <label for="actividad">Actividad</label>
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="basic-addon1"><i class="fas fa-cubes"></i></span>
                        </div>
                        <input type="text" class="form-control" id="actividad" name="actividad" >
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12">
                    <div class="form-group">
                      <label for="domicilio_legal">Domicilio_legal</label>
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="basic-addon1"><i class="fas fa-cubes"></i></span>
                        </div>
                        <input type="text" class="form-control" id="domicilio_legal" name="domicilio_legal" >
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12 col-md-6">
                    <div class="form-group">
                      <label for="provincia">Provincia/Departamento</label>
                      <select name="provincia" id="provincia" class="custom-select">
                        <option value="" selected>Seleccione Provincia/Departamento</option>
                        <option value="Lima/Lima">Lima/Lima</option>
                        <option value="Barranca/Lima">Barranca/Lima</option>
                        <option value="Huaura/Lima">Huaura/Lima</option>
                        <option value="Cajatambo/Lima">Cajatambo/Lima</option>
                        <option value="Oyon/Lima">Oyon/Lima</option>
                        <option value="Huaral/Lima">Huaral/Lima</option>
                        <option value="Canta/Lima">Canta/Lima</option>
                        <option value="Huarochiri/Lima">Huarochiri/Lima</option>
                        <option value="Yauyos/Lima">Yauyos/Lima</option>
                        <option value="Cañete/Lima">Cañete/Lima</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-12 col-md-6">
                    <div class="form-group">
                      <label for="distrito">Distrito</label>
                      <select name="distrito" id="distrito" class="custom-select">
                        <option value="" selected>Seleccione Distrito</option>
                        <option value="Ancon">Ancon</option>
                        <option value="Ate">Ate</option>
                        <option value="Barranco">Barranco</option>
                        <option value="Bellavista">Bellavista</option>
                        <option value="Breña">Breña</option>
                        <option value="Callao">Callao</option>
                        <option value="Carabayllo">Carabayllo</option>
                        <option value="Carmen de la Legua">Carmen de la Legua</option>
                        <option value="Chacaclayo">Chacaclayo</option>
                        <option value="Chorrillos">Chorrillos</option>
                        <option value="Cieneguilla">Cieneguilla</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12 col-md-12">
                    <h5>Personas de Contacto</h5>
                  </div>
                  <div class="col-12 col-md-4">
                    <div class="form-group">
                      <label for="persona1">Persona 1</label>
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="basic-addon1"><i class="fas fa-cubes"></i></span>
                        </div>
                        <input type="text" class="form-control" id="persona1" name="persona1" >
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-md-4">
                    <div class="form-group">
                      <label for="correo1">Correo</label>
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="basic-addon1"><i class="fas fa-cubes"></i></span>
                        </div>
                        <input type="text" class="form-control" id="correo1" name="correo1" >
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-md-4">
                    <div class="form-group">
                      <label for="telefono1">Telefono</label>
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="basic-addon1"><i class="fas fa-cubes"></i></span>
                        </div>
                        <input type="text" class="form-control" id="telefono1" name="telefono1" >
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-md-4">
                    <div class="form-group">
                      <label for="persona2">Persona 2</label>
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="basic-addon1"><i class="fas fa-cubes"></i></span>
                        </div>
                        <input type="text" class="form-control" id="persona2" name="persona2" >
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-md-4">
                    <div class="form-group">
                      <label for="correo2">Correo</label>
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="basic-addon1"><i class="fas fa-cubes"></i></span>
                        </div>
                        <input type="text" class="form-control" id="correo2" name="correo2" >
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-md-4">
                    <div class="form-group">
                      <label for="telefono2">Telefono</label>
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="basic-addon1"><i class="fas fa-cubes"></i></span>
                        </div>
                        <input type="text" class="form-control" id="telefono2" name="telefono2" >
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-12 col-md-6">
                <h4>Datos del Evento</h4>
                <div class="row">
                  <div class="col-12">
                    <div class="form-group">
                      <label for="nombre_instalacion">Nombre de la instalación</label>
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="basic-addon1"><i class="fas fa-cubes"></i></span>
                        </div>
                        <input type="text" class="form-control" id="nombre_instalacion" name="nombre_instalacion" >
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12 col-md-6">
                    <div class="form-group">
                      <label for="hora_inicio">Hora de Inicio</label>
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="basic-addon1"><i class="fas fa-cubes"></i></span>
                        </div>
                        <input type="time" class="form-control" id="hora_inicio" name="hora_inicio" >
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-md-6">
                    <div class="form-group">
                      <label for="hora_termino">Hora de Término</label>
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="basic-addon1"><i class="fas fa-cubes"></i></span>
                        </div>
                        <input type="time" class="form-control" id="hora_termino" name="hora_termino" >
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12 col-md-12">
                    <div class="form-group">
                      <label for="area_afectada">Área afectada (m2)</label>
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="basic-addon1"><i class="fas fa-cubes"></i></span>
                        </div>
                        <input type="number" class="form-control" id="area_afectada" name="area_afectada" >
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-8 col-md-8">
                    <div class="form-group">
                      <label for="cantidad_derramada">Cantidad Derramada</label>
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="basic-addon1"><i class="fas fa-cubes"></i></span>
                        </div>
                        <input type="number" class="form-control" id="cantidad_derramada" name="cantidad_derramada" >
                      </div>
                    </div>
                  </div>
                  <div class="col-4 col-md-4">
                    <div class="form-group">
                      <label for="unidad_derramada">Unidad</label>
                      <select name="unidad_derramada" id="unidad_derramada" class="custom-select">
                        <option value="" selected>Selecciones Unidad</option>
                        <option value="Galon">Galón</option>
                        <option value="Litros">Litros</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-inverse waves-effect waves-light" data-dismiss="modal">Cerrar</button>
          <button id="con-registrar-emergencias" type="button" class="btn btn-success left-margin">Confirmar</button>
        </div>

      </div>
    </div>
  </div>
</div>