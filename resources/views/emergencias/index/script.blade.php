@section('script')

    <!-- Bootstrap tether Core JavaScript -->
    <script src="assets/node_modules/popper/popper.min.js"></script>

    <!-- DatePicker -->
    <script src="assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>

    <!-- This is data table -->
    <script src="assets/node_modules/datatables/jquery.dataTables.min.js"></script>
    <!-- start - This is for export functionality only -->
    <script src="assets/node_modules/datatable-button/dataTables.buttons.min.js"></script>
    <script src="assets/node_modules/datatable-button/buttons.flash.min.js"></script>
    <script src="assets/node_modules/datatable-button/jszip.min.js"></script>
    <script src="assets/node_modules/datatable-button/pdfmake.min.js"></script>
    <script src="assets/node_modules/datatable-button/vfs_fonts.js"></script>
    <script src="assets/node_modules/datatable-button/buttons.html5.min.js"></script>
    <script src="assets/node_modules/datatable-button/buttons.print.min.js"></script>


    <script>
        $(function () {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var opcion_actual = '';
            var emergencias_actual = '';

            $("#bm_emergencias").addClass('active');
            $("#bm_grupo_emergencias").addClass('active');


            var tabla_emergencias = $('#tabla_emergencias').DataTable({
                'paging': true,
                'lengthChange': true,
                'searching': true,
                'ordering': true,
                'info': true,
                'autoWidth': true,
                rowReorder: false,
                responsive: true,
                "language": {
                    "lengthMenu": "Mostrando _MENU_ ficheros por página",
                    "zeroRecords": "No se encontraron ficheros",
                    "info": "Mostrando páginas del _PAGE_ al _PAGES_",
                    "infoEmpty": "No hay ficheros disponibles",
                    "infoFiltered": "(Filtrado de un total _MAX_ de ficheros)",
                    "search": "Buscar",
                    "prev": "Anterior",
                    "paginate": {
                        "first": "First",
                        "last": "Last",
                        "next": ">",
                        "previous": "<"
                    },
                },
                dom: 'Bfrtip',
                buttons: [
                    'csv', 'excel', 'pdf', 'print'
                ],
                fnInitComplete: function () {
                    $('.dataTables_filter input').parent().append('<span id="limpiar_buscador" class="btn btn-xs">X</span>');

                    $("#limpiar_buscador").click(function () {

                        $(".dataTables_filter input").val("");
                        tabla_emergencias.search('')
                            .columns().search('')
                            .draw();

                    });
                }
            });


            //Funcion crear Emergencias
            $("body").on("click", "#btn-crear-emergencias", function () {

                desbloquear();

                $("#titulo-emergencias").text('Reporte Preliminar de Emergencias Ambientales');

                opcion_actual = 'crear';

                $("#modal-emergencias").modal();

            });


            //Funcion editar Emergencias
            $("body").on("click", ".btn-editar-emergencias", function () {

                desbloquear();

                $("#titulo-emergencias").text('Editar Reporte Preliminar de Emergencias Ambientales');

                opcion_actual = 'editar';

                emergencias_actual = $(this).parent().parent().children().eq(1).text();

                $.ajax({
                    url: 'api/emergencias/' + emergencias_actual,
                    type: 'get',
                    beforeSend: function (a) {
                        $(".preloader").show();
                    },
                    complete: function (a) {
                        $(".preloader").hide();
                    },
                    error: function (data) {

                        $(".preloader").hide();
                        swal("Error", "Por favor comuniquese con el administrador", 'error');

                    },
                    success: function (salida) {
                        console.log(salida);

                        if (salida.tipo === "ok") {

                            console.log(salida.mensaje);

                            var elem = ["razon_social", "subsector", "actividad", "domicilio_legal", "distrito", "provincia", "persona1", "persona2", "correo1", "correo2", "telefono1", "telefono2", "nombre_instalacion", "hora_inicio", "hora_termino", "area_afectada", "cantidad_derramada", "unidad_derramada"];
                            
                            elem.forEach(function(item, index) {
                                $("#"+item).val(salida.mensaje[item]);
                            });
                            
                            
                            //$("#nombre-emergencias").val(salida.mensaje.emergencias_nombre);
                            

                            $("#modal-emergencias").modal();

                        } else {
                            swal({
                                title: "Error",
                                text: salida.mensaje,
                                html: true,
                                type: "warning"
                            });
                        }


                    }
                });

            });



            //Funcion alta Emergencias
            $("body").on("click", ".btn-alta-emergencias", function () {

                emergencias_actual = $(this).parent().parent().children().eq(1).text();

                emergencias_estado = 1;

                $.ajax({
                    url: 'api/emergencias_update/' + emergencias_actual,
                    data: {
                        emergencias_estado: emergencias_estado
                    },
                    type: 'post',
                    beforeSend: function (a) {
                        $(".preloader").show();
                    },
                    complete: function (a) {
                        $(".preloader").hide();
                    },
                    error: function (data) {

                        $(".preloader").hide();
                        swal("Error", "Por favor comuniquese con el administrador", 'error');

                    },
                    success: function (salida) {
                        console.log(salida);

                        if(salida.tipo == 'ok'){

                            alerta("Completado",salida.mensaje,"success");

                        }
                        else{

                            swal("Error",salida.mensaje,"warning");

                        }
                    }
                });

            });

            //Funcion baja Emergencias
            $("body").on("click", ".btn-baja-emergencias", function () {

                emergencias_actual = $(this).parent().parent().children().eq(1).text();

                emergencias_estado = 0;

                $.ajax({
                    url: 'api/emergencias_update/' + emergencias_actual,
                    data: {
                        emergencias_estado: emergencias_estado
                    },
                    type: 'post',
                    beforeSend: function (a) {
                        $(".preloader").show();
                    },
                    complete: function (a) {
                        $(".preloader").hide();
                    },
                    error: function (data) {

                        $(".preloader").hide();
                        swal("Error", "Por favor comuniquese con el administrador", 'error');

                    },
                    success: function (salida) {
                        console.log(salida);

                        if(salida.tipo == 'ok'){

                            alerta("Completado",salida.mensaje,"success");

                        }
                        else{
                            swal("Error",salida.mensaje,"warning")
                        }
                    }
                });


            });

            $("body").on("click", "#con-registrar-emergencias", function () {

                var formulario = new FormData($("#formulario_registrar_emergencias")[0]);

                formulario.append("emergencias_estado",1);

/*
                var documento_id = $("#documento-id").val();
                var documento_emergencias = $("#documento-emergencias").val();
                var nombre = $("#nombre-emergencias").val();
                var apellido = $("#apellido-emergencias").val();

                var telefono = $("#telefono-emergencias").val();
                var direccion = $("#direccion-emergencias").val();
                var correo = $("#correo-emergencias").val();

                if (documento_id == '' || documento_emergencias == '' || nombre == '' || apellido == '' || telefono == '' || direccion == '' || correo == '') {

                    swal("Error", "Complete todos los campos", "warning");
                    return;

                }

                else if (!validar_email($("#correo-emergencias").val())) {

                    swal("Error", "Ingrese un correo Válido", "warning");
                    return;

                }
*/

                switch (opcion_actual) {

                    case 'crear':


                        console.log("hasta aqui");
                        $.ajax({
                            url: 'api/emergencias',
                            data: formulario,
                            type: 'post',
                            processData: false,
                            contentType: false,
                            beforeSend: function (a) {
                                $(".preloader").show();
                            },
                            complete: function (a) {
                                $(".preloader").hide();
                            },
                            error: function (data) {

                                $(".preloader").hide();
                                swal("Error", "Por favor comuniquese con el ADMIN", 'error');

                            },
                            success: function (salida) {

                                if (salida.tipo == 'ok') {

                                    alerta("Completado", "Emergencias Creado", "success");

                                }
                                else {
                                    swal("Error", salida, "warning")
                                }
                            }
                        });


                        break;

                    case 'editar':

                        //formulario.append('emergencias_actual', emergencias_actual);

                        $.ajax({
                            url: 'api/emergencias_update/' + emergencias_actual,
                            data: formulario,
                            type: 'post',
                            processData: false,
                            contentType: false,
                            beforeSend: function (a) {
                                $(".preloader").show();
                            },
                            complete: function (a) {
                                $(".preloader").hide();
                            },
                            error: function (data) {

                                $(".preloader").hide();
                                swal("Error", "Por favor comuniquese con el administrador", 'error');

                            },
                            success: function (salida) {
                                console.log(salida);

                                if (salida.tipo == 'ok') {

                                    alerta("Completado", "Emergencias Actualizado", "success");

                                }
                                else {
                                    swal("Error", salida, "warning")
                                }
                            }
                        });

                        break;

                    default:
                        break;

                }

            });


            //Funciones Generales
            function desbloquear() {

                $("#modal-emergencias input,select").val('').attr("disabled", false).parent().parent().show();

                $("#modal-emergencias .box-footer").show();

            }


        });

    </script>
@endsection
