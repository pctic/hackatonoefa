@extends('layouts.body_master')

@include("emergencias.index.head")
@include("emergencias.index.script")

@section("titulo","Emergencias Ambientales") 
@section("boton")
<button id="btn-crear-emergencias" type="button" class="btn btn-info d-none d-lg-block m-l-15 pull-right">
  <i class="fa fa-plus-circle"></i> Crear 
</button>
@endsection

@section('content')

<section class="content">
  <div class="row">

    <div class="col-md-12">
      <!-- Input addon -->
      <div class="card ">

        <div class="card-body">


          <div class="clearfix"></div>

          <span class="pull-right"></span>

          <table id="tabla_emergencias" class="table table-bordered table-hover">
            <thead class="cabeza_emergencias">
              <tr>
                <th>#</th>
                <th hidden>emergencias_id</th>
                
                <th>razon_social</th>
                
                <th>Fecha Creación</th>
                <th>Estado</th>
                <th>Opciones</th>

              </tr>
            </thead>
            <tbody class="cuerpo_emergencias">

              @foreach ($lista_emergencias as $key => $value)

                <tr>
                  <td>{{$key+1}}</td>
                  <td hidden>{{$value->emergencias_id}}</td>

                  <td>{{$value->razon_social}}</td>

                  <td>{{ carbon\Carbon::parse($value->created_at)->format("d-m-Y") }}</td>
                  <td>

                    @if ($value->emergencias_estado == 1)

                      <button class="btn btn-xs btn-success">Activo</button> 

                    @elseif($value->emergencias_estado == 0)

                      <button class=" btn btn-xs btn-danger">Inactivo</button> 
                    
                    @endif

                  </td>
                  <td>

                      <button title="Editar" class="btn-editar-emergencias btn btn-xs btn-warning">
                        <i class="fas fa-pencil-alt"></i>
                      </button>
                      
                      @if ($value->emergencias_estado == 1)

                        <button title="Dar Baja " class="btn-baja-emergencias btn btn-xs btn-danger">
                          <i class="fas fa-arrow-circle-down"></i>
                        </button>
                        
                      @elseif( $value->emergencias_estado == 0 )

                        <button title="Dar Alta" class="btn-alta-emergencias btn btn-xs btn-success">
                          <i class="fas fa-arrow-circle-up"></i>
                        </button>
                        
                      @endif

                  </td>

                </tr>

              @endforeach

            </tbody>

          </table>
        </div>

        <!-- /input-group -->
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>

</section>

@include("emergencias.elemento.modal")

@endsection