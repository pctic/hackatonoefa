<!DOCTYPE html>
<html lang="es">
    @include('login.index.head')

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Wydnex TITULO</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <section id="wrapper" class="login-register login-sidebar" style="background-image:url(assets/images/background/oefa_wallpaper_1.jpg);">
        <div class="login-box card">
            <div class="card-body">
                <form class="form-horizontal form-material" id="loginform" action="login" method="POST">

                        {{ csrf_field() }}


                    

                    <div class="text-center">

                        <a href="javascript:void(0)" class="text-center db">
                            <img style="max-width:150px" src="assets/images/favicon.png" alt="Home" /><br/>
                            {{-- <span style="color:black">NOOVUS</span> --}}
                        </a>

                    </div>

                    @if ( $login_session != "" )
                        <br>
                        <div class="alert alert-{{$login_session['tipo']}}"> {{ $login_session['mensaje'] }} </div>

                    @endif

                    @if ( $register_session != "" )

                        <div class="alert alert-{{$register_session['tipo']}}"> {{ $register_session['mensaje'] }}
                        </div>

                    @endif

                    <div class="form-group m-t-40">
                        <div class="col-xs-12">
                            <input class="form-control" name="name" type="text" placeholder="Usuario" autocomplete="off">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control" name="password" type="password" placeholder="Contraseña" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <div class="custom-control custom-checkbox text-center">
                                <!--
                                <input type="checkbox" class="custom-control-input" id="customCheck1">
                                <label class="custom-control-label" for="customCheck1">Remember me</label>
                                -->
                                {{-- <a style="float:none" href="javascript:void(0)" id="olvido_contra" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i> ¿Olvidó Contraseña?</a> --}}
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-info btn-lg btn-block text-uppercase btn-rounded" type="submit">Ingresar</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
                            <div class="social">
                                <!--
                                <a href="javascript:void(0)" class="btn  btn-facebook" data-toggle="tooltip" title="Login with Facebook"> <i aria-hidden="true" class="fa fa-facebook"></i> </a>
                                <a href="javascript:void(0)" class="btn btn-googleplus" data-toggle="tooltip" title="Login with Google"> <i aria-hidden="true" class="fa fa-google-plus"></i> </a>
                                -->
                            </div>
                        </div>
                    </div>
                    <div class="form-group m-b-0">
                        <div class="col-sm-12 text-center">
                            <!--
                            Don't have an account? <a href="pages-register2.html" class="text-primary m-l-5"><b>Sign Up</b></a>
                            -->
                        </div>
                    </div>
                </form>

                <!------------------------->
                <!--Recuperar Contraseña -->
                <!------------------------->

                <form class="form-horizontal" id="recoverform" action="forgot_password" method="post">

                    {{ csrf_field() }}
                    
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <h3>Recuperar Contraseña</h3> <span id="contra_login" class="btn btn-xs btn-info"> <i class="fa fa-arrow-circle-left"></i> Login </span>
                            <p class="text-muted"> </p>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" name="correo" required="" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    @include('login.index.script')

</body>
