@extends('layouts.body_master')

@include("redes.index.head")
@include("redes.index.script")
 
@section('content')

<section class="content text-center">
  <div class="row">

    <div class="col-md-12">
      
    <iframe width="1200" height="600" src="https://app.powerbi.com/view?r=eyJrIjoiODdkMDI5NmMtN2EyMy00MmY2LTljODctNzQ3MWFiZDU3MmE4IiwidCI6ImM0YTY2YzM0LTJiYjctNDUxZi04YmUxLWIyYzI2YTQzMDE1OCIsImMiOjR9" frameborder="0" allowFullScreen="true"></iframe>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>

</section>

@endsection