@section('script')

    <!-- Bootstrap tether Core JavaScript -->
    <script src="assets/node_modules/popper/popper.min.js"></script>

    <!-- DatePicker -->
    <script src="assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>

    <!-- This is data table -->
    <script src="assets/node_modules/datatables/jquery.dataTables.min.js"></script>
    <!-- start - This is for export functionality only -->
    <script src="assets/node_modules/datatable-button/dataTables.buttons.min.js"></script>
    <script src="assets/node_modules/datatable-button/buttons.flash.min.js"></script>
    <script src="assets/node_modules/datatable-button/jszip.min.js"></script>
    <script src="assets/node_modules/datatable-button/pdfmake.min.js"></script>
    <script src="assets/node_modules/datatable-button/vfs_fonts.js"></script>
    <script src="assets/node_modules/datatable-button/buttons.html5.min.js"></script>
    <script src="assets/node_modules/datatable-button/buttons.print.min.js"></script>
    
@endsection
