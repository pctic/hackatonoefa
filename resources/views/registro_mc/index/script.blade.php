@section('script')

    <!-- Bootstrap tether Core JavaScript -->
    <script src="assets/node_modules/popper/popper.min.js"></script>

    <!-- DatePicker -->
    <script src="assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>

    <!-- This is data table -->
    <script src="assets/node_modules/datatables/jquery.dataTables.min.js"></script>
    <!-- start - This is for export functionality only -->
    <script src="assets/node_modules/datatable-button/dataTables.buttons.min.js"></script>
    <script src="assets/node_modules/datatable-button/buttons.flash.min.js"></script>
    <script src="assets/node_modules/datatable-button/jszip.min.js"></script>
    <script src="assets/node_modules/datatable-button/pdfmake.min.js"></script>
    <script src="assets/node_modules/datatable-button/vfs_fonts.js"></script>
    <script src="assets/node_modules/datatable-button/buttons.html5.min.js"></script>
    <script src="assets/node_modules/datatable-button/buttons.print.min.js"></script>


    <script>
        $(function () {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var opcion_actual = '';
            var registro_mc_actual = '';

            $("#bm_registro_mc").addClass('active');
            $("#bm_grupo_registro_mc").addClass('active');


            var tabla_registro_mc = $('#tabla_registro_mc').DataTable({
                'paging': true,
                'lengthChange': true,
                'searching': true,
                'ordering': true,
                'info': true,
                'autoWidth': true,
                rowReorder: false,
                responsive: true,
                "language": {
                    "lengthMenu": "Mostrando _MENU_ ficheros por página",
                    "zeroRecords": "No se encontraron ficheros",
                    "info": "Mostrando páginas del _PAGE_ al _PAGES_",
                    "infoEmpty": "No hay ficheros disponibles",
                    "infoFiltered": "(Filtrado de un total _MAX_ de ficheros)",
                    "search": "Buscar",
                    "prev": "Anterior",
                    "paginate": {
                        "first": "First",
                        "last": "Last",
                        "next": ">",
                        "previous": "<"
                    },
                },
                dom: 'Bfrtip',
                buttons: [
                    'csv', 'excel', 'pdf', 'print'
                ],
                fnInitComplete: function () {
                    $('.dataTables_filter input').parent().append('<span id="limpiar_buscador" class="btn btn-xs">X</span>');

                    $("#limpiar_buscador").click(function () {

                        $(".dataTables_filter input").val("");
                        tabla_registro_mc.search('')
                            .columns().search('')
                            .draw();

                    });
                }
            });


            //Funcion crear Registro_mc
            $("body").on("click", "#btn-crear-registro_mc", function () {

                desbloquear();

                $("#titulo-registro_mc").text('Registrar Registro_mc');

                opcion_actual = 'crear';

                $("#modal-registro_mc").modal();

            });


            //Funcion editar Registro_mc
            $("body").on("click", ".btn-editar-registro_mc", function () {

                desbloquear();

                $("#titulo-registro_mc").text('Editar Registro_mc');

                opcion_actual = 'editar';

                registro_mc_actual = $(this).parent().parent().children().eq(1).text();

                $.ajax({
                    url: 'api/registro_mc/' + registro_mc_actual,
                    type: 'get',
                    beforeSend: function (a) {
                        $(".preloader").show();
                    },
                    complete: function (a) {
                        $(".preloader").hide();
                    },
                    error: function (data) {

                        $(".preloader").hide();
                        swal("Error", "Por favor comuniquese con el administrador", 'error');

                    },
                    success: function (salida) {
                        console.log(salida);

                        if (salida.tipo === "ok") {

                            console.log(salida.mensaje);

                            $("#numero_mc").val(salida.mensaje.numero_mc);

$("#monto_sancion").val(salida.mensaje.monto_sancion);

$("#resolucion").val(salida.mensaje.resolucion);

$("#expediente").val(salida.mensaje.expediente);

$("#administrado").val(salida.mensaje.administrado);

$("#ubicacion").val(salida.mensaje.ubicacion);

$("#sector").val(salida.mensaje.sector);

$("#materia").val(salida.mensaje.materia);

$("#medidas_correctivas").val(salida.mensaje.medidas_correctivas);

$("#fecha_limite").val(salida.mensaje.fecha_limite);



                            //$("#nombre-registro_mc").val(salida.mensaje.registro_mc_nombre);
                            


                            $("#modal-registro_mc").modal();

                        } else {
                            swal({
                                title: "Error",
                                text: salida.mensaje,
                                html: true,
                                type: "warning"
                            });
                        }


                    }
                });

            });

            //Funcion alta Registro_mc
            $("body").on("click", ".btn-alta-registro_mc", function () {

                registro_mc_actual = $(this).parent().parent().children().eq(1).text();

                registro_mc_estado = 1;

                $.ajax({
                    url: 'api/registro_mc_update/' + registro_mc_actual,
                    data: {
                        registro_mc_estado: registro_mc_estado
                    },
                    type: 'post',
                    beforeSend: function (a) {
                        $(".preloader").show();
                    },
                    complete: function (a) {
                        $(".preloader").hide();
                    },
                    error: function (data) {

                        $(".preloader").hide();
                        swal("Error", "Por favor comuniquese con el administrador", 'error');

                    },
                    success: function (salida) {
                        console.log(salida);

                        if(salida.tipo == 'ok'){

                            alerta("Completado",salida.mensaje,"success");

                        }
                        else{

                            swal("Error",salida.mensaje,"warning");

                        }
                    }
                });

            });

            //Funcion baja Registro_mc
            $("body").on("click", ".btn-baja-registro_mc", function () {

                registro_mc_actual = $(this).parent().parent().children().eq(1).text();

                registro_mc_estado = 0;

                $.ajax({
                    url: 'api/registro_mc_update/' + registro_mc_actual,
                    data: {
                        registro_mc_estado: registro_mc_estado
                    },
                    type: 'post',
                    beforeSend: function (a) {
                        $(".preloader").show();
                    },
                    complete: function (a) {
                        $(".preloader").hide();
                    },
                    error: function (data) {

                        $(".preloader").hide();
                        swal("Error", "Por favor comuniquese con el administrador", 'error');

                    },
                    success: function (salida) {
                        console.log(salida);

                        if(salida.tipo == 'ok'){

                            alerta("Completado",salida.mensaje,"success");

                        }
                        else{
                            swal("Error",salida.mensaje,"warning")
                        }
                    }
                });


            });

            $("body").on("click", "#con-registrar-registro_mc", function () {

                var formulario = new FormData($("#formulario_registrar_registro_mc")[0]);

                formulario.append("registro_mc_estado",1);

/*
                var documento_id = $("#documento-id").val();
                var documento_registro_mc = $("#documento-registro_mc").val();
                var nombre = $("#nombre-registro_mc").val();
                var apellido = $("#apellido-registro_mc").val();

                var telefono = $("#telefono-registro_mc").val();
                var direccion = $("#direccion-registro_mc").val();
                var correo = $("#correo-registro_mc").val();

                if (documento_id == '' || documento_registro_mc == '' || nombre == '' || apellido == '' || telefono == '' || direccion == '' || correo == '') {

                    swal("Error", "Complete todos los campos", "warning");
                    return;

                }

                else if (!validar_email($("#correo-registro_mc").val())) {

                    swal("Error", "Ingrese un correo Válido", "warning");
                    return;

                }
*/

                switch (opcion_actual) {

                    case 'crear':


                        console.log("hasta aqui");
                        $.ajax({
                            url: 'api/registro_mc',
                            data: formulario,
                            type: 'post',
                            processData: false,
                            contentType: false,
                            beforeSend: function (a) {
                                $(".preloader").show();
                            },
                            complete: function (a) {
                                $(".preloader").hide();
                            },
                            error: function (data) {

                                $(".preloader").hide();
                                swal("Error", "Por favor comuniquese con el ADMIN", 'error');

                            },
                            success: function (salida) {

                                if (salida.tipo == 'ok') {

                                    alerta("Completado", "Registro_mc Creado", "success");

                                }
                                else {
                                    swal("Error", salida, "warning")
                                }
                            }
                        });


                        break;

                    case 'editar':

                        //formulario.append('registro_mc_actual', registro_mc_actual);

                        $.ajax({
                            url: 'api/registro_mc_update/' + registro_mc_actual,
                            data: formulario,
                            type: 'post',
                            processData: false,
                            contentType: false,
                            beforeSend: function (a) {
                                $(".preloader").show();
                            },
                            complete: function (a) {
                                $(".preloader").hide();
                            },
                            error: function (data) {

                                $(".preloader").hide();
                                swal("Error", "Por favor comuniquese con el administrador", 'error');

                            },
                            success: function (salida) {
                                console.log(salida);

                                if (salida.tipo == 'ok') {

                                    alerta("Completado", "Registro_mc Actualizado", "success");

                                }
                                else {
                                    swal("Error", salida, "warning")
                                }
                            }
                        });

                        break;

                    default:
                        break;

                }

            });


            //Funciones Generales
            function desbloquear() {

                $("#modal-registro_mc input,select").val('').attr("disabled", false).parent().parent().show();

                $("#modal-registro_mc .box-footer").show();

            }


        });

    </script>
@endsection
