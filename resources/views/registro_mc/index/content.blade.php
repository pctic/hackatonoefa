@extends('layouts.body_master')

@include("registro_mc.index.head")
@include("registro_mc.index.script")

@section("titulo","Registro_mc Registrados") 
@section("boton")
<button id="btn-crear-registro_mc" type="button" class="btn btn-info d-none d-lg-block m-l-15 pull-right">
  <i class="fa fa-plus-circle"></i> Crear 
</button>
@endsection

@section('content')

<section class="content">
  <div class="row">

    <div class="col-md-12">
      <!-- Input addon -->
      <div class="card ">

        <div class="card-body">


          <div class="clearfix"></div>

          <span class="pull-right"></span>

          <table id="tabla_registro_mc" class="table table-bordered table-hover">
            <thead class="cabeza_registro_mc">
              <tr>
                <th>#</th>
                <th hidden>registro_mc_id</th>
                
                <th>resolucion</th>
                
                <th>Fecha Creación</th>
                <th>Estado</th>
                <th>Opciones</th>

              </tr>
            </thead>
            <tbody class="cuerpo_registro_mc">

              @foreach ($lista_registro_mc as $key => $value)

                <tr>
                  <td>{{$key+1}}</td>
                  <td hidden>{{$value->registro_mc_id}}</td>

                  <td>{{$value->resolucion}}</td>

                  <td>{{ carbon\Carbon::parse($value->created_at)->format("d-m-Y") }}</td>
                  <td>

                    @if ($value->registro_mc_estado == 1)

                      <button class="btn btn-xs btn-success">Activo</button> 

                    @elseif($value->registro_mc_estado == 0)

                      <button class=" btn btn-xs btn-danger">Inactivo</button> 
                    
                    @endif

                  </td>
                  <td>

                      <button title="Editar" class="btn-editar-registro_mc btn btn-xs btn-warning">
                        <i class="fas fa-pencil-alt"></i>
                      </button>
                      
                      @if ($value->registro_mc_estado == 1)

                        <button title="Dar Baja " class="btn-baja-registro_mc btn btn-xs btn-danger">
                          <i class="fas fa-arrow-circle-down"></i>
                        </button>
                        
                      @elseif( $value->registro_mc_estado == 0 )

                        <button title="Dar Alta" class="btn-alta-registro_mc btn btn-xs btn-success">
                          <i class="fas fa-arrow-circle-up"></i>
                        </button>
                        
                      @endif

                  </td>

                </tr>

              @endforeach

            </tbody>

          </table>
        </div>


        <!-- /input-group -->
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>

</section>

@include("registro_mc.elemento.modal")

@endsection