<div class="modal fade" id="modal-registro_mc">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="box box-primary">
        <div class="modal-header with-border">
          <h3 class="box-title" id="titulo-registro_mc">Registrar Registro_mc</h3>
        </div>
        <div class="modal-body">

          <form id="formulario_registrar_registro_mc" type="post" autocomplete="off" action="javascript:void(0);">




            <div class="row">

              <div class="col-12">
      
                <div class="form-group">
    
                <label for="registro_mc_numero_mc">Numero_mc</label>
    
                <div class="input-group">
    
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"><i class="fas fa-cube"></i></span>
                    </div>
    
                    <input type="number" class="form-control" id="numero_mc" name="numero_mc" >
    
                </div>
    
                </div>
    
            </div><div class="col-12">
      
                <div class="form-group">
    
                <label for="registro_mc_monto_sancion">Monto_sancion</label>
    
                <div class="input-group">
    
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"><i class="fas fa-cube"></i></span>
                    </div>
    
                    <input type="number" class="form-control" id="monto_sancion" name="monto_sancion" >
    
                </div>
    
                </div>
    
            </div><div class="col-12">
      
                <div class="form-group">
    
                <label for="resolucion">Resolucion</label>
    
                <div class="input-group">
    
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"><i class="fas fa-cubes"></i></span>
                    </div>
    
                    <input type="text" class="form-control" id="resolucion" name="resolucion" >
    
                </div>
    
                </div>
    
            </div><div class="col-12">
      
                <div class="form-group">
    
                <label for="expediente">Expediente</label>
    
                <div class="input-group">
    
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"><i class="fas fa-cubes"></i></span>
                    </div>
    
                    <input type="text" class="form-control" id="expediente" name="expediente" >
    
                </div>
    
                </div>
    
            </div><div class="col-12">
      
                <div class="form-group">
    
                <label for="administrado">Administrado</label>
    
                <div class="input-group">
    
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"><i class="fas fa-cubes"></i></span>
                    </div>
    
                    <input type="text" class="form-control" id="administrado" name="administrado" >
    
                </div>
    
                </div>
    
            </div><div class="col-12">
      
                <div class="form-group">
    
                <label for="ubicacion">Ubicacion</label>
    
                <div class="input-group">
    
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"><i class="fas fa-cubes"></i></span>
                    </div>
    
                    <input type="text" class="form-control" id="ubicacion" name="ubicacion" >
    
                </div>
    
                </div>
    
            </div><div class="col-12">
      
                <div class="form-group">
    
                <label for="sector">Sector</label>
    
                <div class="input-group">
    
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"><i class="fas fa-cubes"></i></span>
                    </div>
    
                    <input type="text" class="form-control" id="sector" name="sector" >
    
                </div>
    
                </div>
    
            </div><div class="col-12">
      
                <div class="form-group">
    
                <label for="materia">Materia</label>
    
                <div class="input-group">
    
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"><i class="fas fa-cubes"></i></span>
                    </div>
    
                    <input type="text" class="form-control" id="materia" name="materia" >
    
                </div>
    
                </div>
    
            </div><div class="col-12">
      
                <div class="form-group">
    
                <label for="medidas_correctivas">Medidas_correctivas</label>
    
                <div class="input-group">
    
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"><i class="fas fa-cubes"></i></span>
                    </div>
    
                    <input type="text" class="form-control" id="medidas_correctivas" name="medidas_correctivas" >
    
                </div>
    
                </div>
    
            </div><div class="col-12">
      
                <div class="form-group">
    
                <label for="fecha_limite">Fecha_limite</label>
    
                <div class="input-group">
    
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"><i class="fas fa-cubes"></i></span>
                    </div>
    
                    <input type="text" class="form-control" id="fecha_limite" name="fecha_limite" >
    
                </div>
    
                </div>
    
            </div>

            </div>
  
  


          </form>

        </div>

        <div class="modal-footer">

          <button type="button" class="btn btn-inverse waves-effect waves-light" data-dismiss="modal">Cerrar</button>

          <button id="con-registrar-registro_mc" type="button" class="btn btn-success left-margin">Confirmar</button>

        </div>

      </div>
    </div>
  </div>
</div>