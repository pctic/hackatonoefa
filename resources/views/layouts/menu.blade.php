<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<aside class="left-sidebar">
    <div class="d-flex no-block nav-text-box align-items-center">
        <span><img src="{{asset('assets/images/logo-icon.png')}}" alt="elegant admin template"></span>
        <a class="nav-lock waves-effect waves-dark ml-auto hidden-md-down" href="javascript:void(0)"><i
                class="mdi mdi-toggle-switch"></i></a>
        <a class="nav-toggler waves-effect waves-dark ml-auto hidden-sm-up" href="javascript:void(0)"><i
                class="ti-menu ti-close"></i></a>
    </div>
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">


                <!-- //////////////////////////////////////////////////////////////////////////////////////////// -->
                <li>
                    <a class="waves-effect waves-dark" href="{{ route("dashboard")}}" aria-expanded="false">
                        <i class="fas fa-sticky-note text-info"></i>
                        <span class="hide-menu">Dashboard</span>
                    </a>
                </li>

                <li>
                    <a class="waves-effect waves-dark" href="{{ route("vista_usuario")}}" aria-expanded="false">
                        <i class="fas fa-list text-success"></i>
                        <span class="hide-menu">Usuarios Registrados</span>
                    </a>
                </li>

                <li>
                    <a class="waves-effect waves-dark" href="{{ route("emergencias_vista")}}" aria-expanded="false">
                        <i class="fas fa-exclamation-circle"></i>
                        <span class="hide-menu">Emergencias Ambientales</span>
                    </a>
                </li>

                <li>
                    <a class="waves-effect waves-dark" href="{{ route("emergencias_registro")}}" aria-expanded="false">
                        <i class="fas fa-exclamation-circle"></i>
                        <span class="hide-menu">Registro de Emergencias</span>
                    </a>
                </li>

                <li>
                    <a class="waves-effect waves-dark" href="{{ route("redes")}}" aria-expanded="false">
                        <i class="fab fa-twitter"></i>
                        <span class="hide-menu">Analisis Twitter</span>
                    </a>
                </li>

                <li>
                    <a class="waves-effect waves-dark" href="{{ route("medidas_correctivas")}}" aria-expanded="false">
                        <i class="fas fa-book"></i>
                        <span class="hide-menu">Medidas Correctivas</span>
                    </a>
                </li>

                <li>
                    <a class="waves-effect waves-dark" href="{{ route("registro_mc_vista")}}" aria-expanded="false">
                        <i class="fas fa-book"></i>
                        <span class="hide-menu">Registro M.C.</span>
                    </a>
                </li>

                <li>
                    <a class="waves-effect waves-dark" href="{{ route("logout")}}" aria-expanded="false">
                        <i class="fas fa-power-off text-danger"></i>
                        <span class="hide-menu">Cerrar Sesión</span>
                    </a>
                </li>


            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
