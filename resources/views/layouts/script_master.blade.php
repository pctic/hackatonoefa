<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
<script src="{!! asset('assets/node_modules/jquery/jquery-3.2.1.js') !!}"></script>
<!-- Bootstrap popper Core JavaScript -->

<script src="{!! asset('assets/node_modules/popper/popper.min.js') !!}"></script>
<script src="{!! asset('assets/node_modules/bootstrap/dist/js/bootstrap.min.js') !!}"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{!! asset('dist/js/perfect-scrollbar.jquery.min.js') !!}"></script>
<!--Wave Effects -->
<script src="{!! asset('dist/js/waves.js') !!}"></script>
<!--Menu sidebar -->
<script src="{!! asset('dist/js/sidebarmenu.js') !!}"></script>
<!--Custom JavaScript -->
<script src="{!! asset('dist/js/custom.min.js') !!}"></script>
<!-- ============================================================== -->
<!-- This page plugins -->
<!-- ============================================================== -->
<!--morris JavaScript -->
{{-- <script src="{!! asset('assets/node_modules/raphael/raphael-min.js') !!}"></script>
<script src="{!! asset('assets/node_modules/morrisjs/morris.min.js') !!}"></script>
<script src="{!! asset('assets/node_modules/jquery-sparkline/jquery.sparkline.min.js') !!}"></script> --}}
<!--c3 JavaScript -->
{{-- <script src="{!! asset('assets/node_modules/d3/d3.min.js') !!}"></script>
<script src="{!! asset('assets/node_modules/c3-master/c3.min.js') !!}"></script> --}}
<!-- Popup message jquery -->
{{-- <script src="{!! asset('assets/node_modules/toast-master/js/jquery.toast.js') !!}"></script> --}}
<!-- Chart JS -->
{{--
<script src="{!! asset('dist/js/dashboard1.js') !!}"></script> --}}

<!-- Sweet Alert -->
<script src="{!! asset('assets/node_modules/sweetalert/sweetalert.min.js') !!}"></script>

<!--Custom Select 2 -->
<script src="{!! asset('assets/node_modules/select2/dist/js/select2.min.js') !!}"></script>

<script src="{!! asset('assets/node_modules/clockpicker/dist/jquery-clockpicker.min.js') !!}"></script>


<script>
    function alerta(titulo, mensaje, tipo) {

        swal({
                title: titulo,
                text: mensaje,
                type: tipo,
                confirmButtonClass: "btn-success",
                confirmButtonText: "Cerrar"
            },
            function () {
                location.reload();
            });

    }

    function validar_email(email) {

        var check = "" + email;
        if ((check.search('@') >= 0) && (check.search(/\./) >= 0))
            if (check.search('@') < check.split('@')[1].search(/\./) + check.search('@')) return true;
            else return false;
        else return false;

    }


    $(function () {


        $("body").on("keydown", ".solo-numeros", function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl/cmd+A
                (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: Ctrl/cmd+C
                (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: Ctrl/cmd+X
                (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });


    });

</script>
