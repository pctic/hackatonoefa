<!-- ============================================================== -->
<!-- User profile and search -->
<!-- ============================================================== -->
<li class="nav-item dropdown">

    <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown"
       aria-haspopup="true"
       aria-expanded="false">

        @if( session("foto_url") != "" )

            <img src="{{ asset("../storage/app/" . session("foto_url") )  }}" alt="user" class="img-circle" width="30">

        @else

            <img src="{{ asset("assets/images/users/1.jpg") }}" alt="user" class="img-circle" width="30">

        @endif

    </a>

    <div class="dropdown-menu dropdown-menu-right user-dd animated flipInY">
        <span class="with-arrow"><span class="bg-dark"></span></span>
        <div class="d-flex no-block align-items-center p-15 bg-dark text-white m-b-10">

            <div class="">


                @if( session("foto_url") != "" )

                    <img src="{{ asset("../storage/app/" . session("foto_url") )  }}" alt="user" class="img-circle"
                         width="60"> @else

                    <img src="{{ asset('assets/images/users/1.jpg') }}" alt="user" class="img-circle" width="60"
                         style="width:50px;height:50px">                @endif

            </div>

            <div class="m-l-10">
                <h4 class="m-b-0">{{session("persona_nombre")}}</h4>
                <p class=" m-b-0">{{session("persona_correo")}}</p>
            </div>

        </div>
        <a class="dropdown-item" href="{{route("vista_perfil")}}"><i class="ti-user m-r-5 m-l-5"></i> Mi Perfil</a>

        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="{{route("logout")}}"><i class="fa fa-power-off m-r-5 m-l-5"></i> Cerrar
            Sesión</a>

    </div>
</li>
<!-- ============================================================== -->
<!-- User profile and search -->
<!-- ============================================================== -->
