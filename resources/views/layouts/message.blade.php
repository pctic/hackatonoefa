<!-- ============================================================== -->
<!-- Messages -->
<!-- ============================================================== -->
<li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" id="2" data-toggle="dropdown"
       aria-haspopup="true" aria-expanded="false"> <i class="icon-note"></i>
        <div class="notify"><span class="heartbit"></span> <span class="point"></span></div>
    </a>
    <div class="dropdown-menu mailbox animated bounceInDown" aria-labelledby="2">
        <span class="with-arrow"><span class="bg-danger"></span></span>
        <ul>
            <li>
                <div class="drop-title text-white bg-danger">
                    <h4 class="m-b-0 m-t-5">0 Nuevos</h4>
                    <span class="font-light">Mensajes</span>
                </div>
            </li>

            <li>
                <a class="nav-link text-center link m-b-5" href="javascript:void(0);"> <b>See all e-Mails</b> <i
                        class="fa fa-angle-right"></i> </a>
            </li>
        </ul>
    </div>
</li>
<!-- ============================================================== -->
<!-- End Messages -->
<!-- ============================================================== -->
