<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- TOKEN CSRF -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{!! asset('assets/images/favicon.png') !!}">
    <title>{{ENV("APP_NAME")}}</title>
    <!-- Custom CSS -->
    {{-- <link href="{!! asset('assets/icons/font-awesome/css/font-awesome.min.css" rel="stylesheet"> --}}
    <link href="{!! asset('dist/css/style.css" rel="stylesheet') !!}">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    @yield("head")

    <link
        href="{!! asset('assets/icons/font-awesome-5/web-fonts-with-css/css/fontawesome-all.min.css" rel="stylesheet') !!}">


    <link href="{!! asset('assets/node_modules/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet') !!}">


    <!-- Sweet Alert -->
    <link rel="stylesheet" href="{!! asset('assets/node_modules/sweetalert/sweetalert.css') !!}">

    <style>

        html body .navbar-dark .mailbox .message-center {

            height: auto !important;

        }


        .oculto {
            display: none;
        }

        .skin-red .topbar {
            background: #ffffff;
            background: -moz-linear-gradient(left, #ffffff 0%, #000000 180%);
            background: -webkit-linear-gradient(left, #ffffff 0%, #000000 180%);
            background: linear-gradient(to right, #ffffff 0%, #000000 180%);
        }

        .topbar .top-navbar .navbar-nav > .nav-item > .nav-link {
            padding-left: 15px;
            color: black;
            padding-right: 15px;
            font-size: 18px;
            line-height: 50px;
        }

        .select2 {
            width: 100% !important;
        }

        .text-bold {
            font-weight: 500;
        }

        .td-red {
            background-color: #f62d51;
            color: white;
        }

        .td-orange {
            background-color: #ffbc34;
            color: white;
        }

        .td-green {
            background-color: #36bea6;
            color: white;
        }

        .td-lightblue {
            background-color: #009efb;
            color: white;
        }

        .td-black {
            background-color: black;
            color: white;
        }

        .prioridad_elegida {
            background: black !important;
            color: white;
        }

        .blank {

            background-color: white !important;
            color: black;

        }


    </style>


</head>
