<!-- ============================================================== -->
<!-- Preloader - style you can find in spinners.css -->
<!-- ============================================================== -->
<div class="preloader" style="background:#00000091">
    <div class="loader">
        <div class="loader__figure" style="border: 0 solid #f80604;"></div>
        <p class="loader__label text-bold" style="color:black;font-size:20px">Noovus</p>
    </div>
</div>