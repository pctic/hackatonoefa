<div class="modal fade" id="modal-usuario">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="box box-primary">
                <div class="modal-header with-border">
                    <h3 class="box-title" id="titulo-usuario">Registrar Usuario</h3>
                </div>
                <div class="modal-body">

                    <form id="formulario_registrar_usuario" type="post" autocomplete="off" action="javascript:void(0);">

                        <div class="row">

                            <div class="col-6">

                                <div class="form-group">

                                    <label for="dni-usuario">DNI ( Ingreso Login )</label>

                                    <div class="input-group">

                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1"><i class="ti  ti-settings"></i></span>
                                        </div>
                                        <input type="text" class="form-control req_registrar solo-numeros" id="dni-usuario" name="dni-usuario" maxlength="8">

                                    </div>

                                </div>

                            </div>

                            

                        </div>

                        <div class="row">

                            <div class="col-6">

                                <div class="form-group">

                                    <label for="password-usuario">Contraseña</label>

                                    <div class="input-group">

                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1"><i class="ti ti-lock"></i></span>
                                        </div>
                                        <input type="password" class="form-control req_registrar" id="password-usuario" name="password-usuario">

                                    </div>

                                </div>

                            </div>

                            <div class="col-6">

                                <div class="form-group">

                                    <label for="password-usuario-validar">Repite Contraseña</label>

                                    <div class="input-group">

                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1"><i class="ti ti-lock"></i></span>
                                        </div>
                                        <input type="password" class="form-control req_registrar" id="password-usuario-validar" name="password-usuario-validar">

                                    </div>

                                </div>

                            </div>

                        </div>





                        <div class="row">

                            <div class="col-6">


                                <div class="form-group">

                                    <label for="nombre-usuario">Nombres</label>

                                    <div class="input-group">

                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1"><i class="ti ti-user"></i></span>
                                        </div>
                                        <input type="text" class="form-control req_registrar" id="nombre-usuario" name="nombre-usuario">

                                    </div>

                                </div>


                            </div>



                            <div class="col-6">

                                <div class="form-group">

                                    <label for="apellido-usuario">Apellidos</label>

                                    <div class="input-group">

                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1"><i class="ti ti-user"></i></span>
                                        </div>
                                        <input type="text" class="form-control req_registrar" id="apellido-usuario" name="apellido-usuario">

                                    </div>
                                </div>


                            </div>

                        </div>



                        <div class="row">

                            <div class="col-6">

                                <div class="form-group">

                                    <label for="correo-usuario">Correo</label>

                                    <div class="input-group">

                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1"><i class="ti ti-email"></i></span>
                                        </div>
                                        <input type="email" class="form-control req_registrar" id="correo-usuario" name="correo-usuario">

                                    </div>

                                </div>


                            </div>

                            <div class="col-6">

                                <div class="form-group">

                                    <label for="telefono-usuario"># de Contacto</label>

                                    <div class="input-group">

                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1"><i class="ti ti-mobile"></i></span>
                                        </div>
                                        <input type="text" class="form-control req_registrar solo-numeros" id="telefono-usuario" name="telefono-usuario" maxlength="9">

                                    </div>

                                </div>

                            </div>

                        </div>


                        <div class="row">

                            <div class="col-12">

                                <div class="form-group">

                                    <label for="direccion-usuario">Dirección</label>

                                    <div class="input-group">

                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1"><i class="ti ti-map-alt"></i></span>
                                        </div>
                                        <input type="text" class="form-control req_registrar" id="direccion-usuario" name="direccion-usuario">

                                    </div>

                                </div>

                            </div>

                        </div>


                        <div class="row perfil_usuario">

                            <div class="col-12">

                                <div class="form-group">

                                    <label for="perfil-usuario">Perfil</label>

                                    <div class="input-group">

                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1"><i class="ti ti-user"></i></span>
                                        </div>

                                        <select class="form-control req_registrar" id="perfil-usuario" name="perfil-usuario">
      
                          @foreach ($perfiles as $p)
      
                              <option value="{{$p['perfil_id']}}"> {{$p['perfil_nombre']}} </option> 
      
                          @endforeach
          
                      </select>

                                    </div>

                                </div>


                            </div>

                        </div>



                    </form>

                </div>

                <div class="modal-footer">

                    <button type="button" class="btn btn-inverse waves-effect waves-light" data-dismiss="modal">Cerrar</button>

                    <button id="con-registrar-usuario" type="button" class="btn btn-success left-margin">Confirmar</button>

                </div>

            </div>
        </div>
    </div>
</div>