<div class="modal fade" id="modal-asignar">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="box box-primary">
                <div class="modal-header with-border">
                    <h3 class="box-title" id="titulo-usuario">Asignar Perfiles</h3>
                </div>
                <div class="modal-body">

                    <form id="formulario_asignar_usuario" type="post" autocomplete="off" action="javascript:void(0);">


                        @foreach ($perfiles as $key =>  $p)

                        <div class="row">

                            <div class="col-12">

                                <div class="form-group perfil_check">

                                    <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input elemento_check" id="check_{{$p->perfil_id}}" value="{{$p->perfil_id}}">
                                        <label class="custom-control-label" for="check_{{$p->perfil_id}}">{{$p->perfil_nombre}}</label>
                                    </div>

                                </div>

                            </div>

                        </div>

                        @endforeach



                    </form>

                </div>

                <div class="modal-footer">

                    <button type="button" class="btn btn-inverse waves-effect waves-light" data-dismiss="modal">Cerrar</button>

                    <button id="con-asignar-usuario" type="button" class="btn btn-success left-margin">Confirmar</button>

                </div>

            </div>
        </div>
    </div>
</div>