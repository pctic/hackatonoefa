@section('script')


<!-- Bootstrap tether Core JavaScript -->
<script src="assets/node_modules/popper/popper.min.js"></script>



<!-- This is data table -->
<script src="assets/node_modules/datatables/jquery.dataTables.min.js"></script>
<!-- start - This is for export functionality only -->
<script src="assets/node_modules/datatable-button/dataTables.buttons.min.js"></script>
<script src="assets/node_modules/datatable-button/buttons.flash.min.js"></script>
<script src="assets/node_modules/datatable-button/jszip.min.js"></script>
<script src="assets/node_modules/datatable-button/pdfmake.min.js"></script>
<script src="assets/node_modules/datatable-button/vfs_fonts.js"></script>
<script src="assets/node_modules/datatable-button/buttons.html5.min.js"></script>
<script src="assets/node_modules/datatable-button/buttons.print.min.js"></script>


<script>
	$(function(){

		$.ajaxSetup({
	      	headers: {
	          	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	      	}
	  	});

		var dateToday = new Date();



		var opcion_actual = '';
		var usuario_actual = '';
		var perfil_actual = '';

		$("#bm_usuarios").addClass('active');
		$("#bm_grupo_usuarios").addClass('active');

		

		var tabla_usuarios = $('#tabla_usuarios').DataTable({
			'paging'      : true,
			'lengthChange': true,
			'searching'   : true,
			'ordering'    : true,
			'info'        : true,
			'autoWidth'   : true,
            rowReorder: {
                selector: 'td:nth-child(2)'
            },
            responsive: true,
			"language": {
				"lengthMenu": "Mostrando _MENU_ ficheros por página",
				"zeroRecords": "No se encontraron ficheros",
				"info": "Mostrando páginas del _PAGE_ al _PAGES_",
				"infoEmpty": "No hay ficheros disponibles",
				"infoFiltered": "(Filtrado de un total _MAX_ de ficheros)",
				"search" : "Buscar",
				"prev" : "Anterior",
				"paginate": {
					"first":      "First",
					"last":       "Last",
					"next":       ">",
					"previous":   "<"
				},
			},
			dom: 'Bfrtip',
			buttons: [
				'csv', 'excel', 'pdf', 'print',
			],
			fnInitComplete: function(){
				$('.dataTables_filter input').parent().append('<span id="limpiar_buscador" class="btn btn-xs">X</span>');

				$("#limpiar_buscador").click(function(){

					$(".dataTables_filter input").val("");
					tabla_usuarios.search( '' )
						.columns().search( '' )
						.draw();

				});
			}
		});
		
		


		function validar_perfil(perfil_actual = 1){



		}




		//Funcion crear usuarios
		$("body").on("click","#btn-crear-usuario",function(){

			desbloquear();

			$(".perfil_usuario").show();

			$("#password-usuario-validar").removeClass("has-danger");

			validar_perfil();

			$("#titulo-usuario").text('Registrar Usuario');

			$("#perfil-usuario").val(1);

			opcion_actual = 'crear';

			$("#perfil_usuario").attr("disabled",false);
			$("#especialidad").attr("disabled",false);
			$("#examen_id").attr("disabled",false);
			

			$("#modal-usuario").modal();

		});
		

		//Funcion editar usuarios
		$("body").on("click",".btn-editar-usuario",function(){
			
			desbloquear();

			// $(".perfil_usuario").hide();

			var perfil_click = $(this).parent().parent().children().eq(2).text();
			
			validar_perfil( perfil_click );

			$("#titulo-usuario").text('Editar Usuario');

			opcion_actual = 'editar';

			usuario_actual = $(this).parent().parent().children().eq(1).text();
			perfil_actual = $(this).parent().parent().children().eq(2).text();


			$.ajax({
				url:'usuario_ver_datos',
				data: {
					usuario_actual:usuario_actual
				},
				type:'post',
				beforeSend:function(a){
				  	$(".preloader").show();
				},
				complete:function(a){
				    $(".preloader").hide();
				},
				error:function (data) {

				    $(".preloader").hide();
				    swal("Error","Por favor comuniquese con el administrador",'error');

				},
				success:function(salida){
					console.log(salida);
				    $("#username-usuario").val(salida.name);
				    $("#nombre-usuario").val(salida.persona_nombre);
				    $("#apellido-usuario").val(salida.persona_apellido);
				    $("#dni-usuario").val(salida.persona_documento);
				    $("#telefono-usuario").val(salida.persona_telefono);
				    $("#perfil-usuario").val(salida.perfil_id);
				    $("#correo-usuario").val(salida.persona_correo);
				    $("#direccion-usuario").val(salida.persona_direccion);
				    $("#empresa-usuario").val(salida.empresa_id);

					$("#perfil-usuario").val(salida.perfil_id);


				    $("#modal-usuario").modal();

				}
			});

		});

		//Funcion alta usuario
		$("body").on("click",".btn-alta-usuario",function(){

			usuario_actual = $(this).parent().parent().children().eq(1).text();

			estado = 1;

			$.ajax({
				url:'usuario_estado',
				data: {
					usuario_actual:usuario_actual,
					estado:estado
				},
				type:'post',
			    beforeSend:function(a){
				    $(".preloader").show();
				},
				complete:function(a){
				    $(".preloader").hide();
				},
				error:function (data) {

				    $(".preloader").hide();
				    swal("Error","Por favor comuniquese con el administrador",'error');

				},
				success:function(salida){

				    if(salida == 'ok'){

				      	alerta("Completado","Usuario Activado","success");

				    }
				    else{
				       	swal("Error",salida,"warning")
				    }
				}
			});

		});

		//Funcion baja usuario
		$("body").on("click",".btn-baja-usuario",function(){

			usuario_actual = $(this).parent().parent().children().eq(1).text();

			estado = 0;

			$.ajax({
				url:'usuario_estado',
				data: {
					usuario_actual:usuario_actual,
					estado:estado
				},
				type:'post',
			    beforeSend:function(a){
				    $(".preloader").show();
				},
				complete:function(a){
				    $(".preloader").hide();
				},
				error:function (data) {

				    $(".preloader").hide();
				    swal("Error","Por favor comuniquese con el administrador",'error');

				},
				success:function(salida){
					console.log(salida);
				    if(salida == 'ok'){

				      	alerta("Completado","Usuario Desactivado","success");

				    }
				    else{
				       	swal("Error",salida,"warning")
				    }
				}
			});


		});

		$("body").on("click","#con-registrar-usuario",function(){

			if( !validar_contra() ){
				
				swal("Error","Las contraseñas no coinciden","warning");
				return;

			}
		

			var formulario = new FormData($("#formulario_registrar_usuario")[0]);

			$correo = $("#correo-usuario").val();
			$contra = $("#password-usuario").val();
			$nombre = $("#nombre-usuario").val();
			$apellido = $("#apellido-usuario").val();
			$dni = $("#dni-usuario").val();
			$telefono = $("#telefono-usuario").val();

			if( $correo == '' || $nombre == '' || $apellido == '' || $dni == '' || $telefono == '' ){

				swal("Error","Complete todos los campos","warning")
				return;

			}

			else if( validar_email( $("#correo").val() ) ){

				swal("Error","Ingrese un correo Válido","warning");
				return;

			}




			switch(opcion_actual){

				case 'crear':

					if( $contra == '' ){

						swal("Error","Ingrese una contraseña","warning")
						return;

					}

					$.ajax({
				      url:'usuario_crear',
				      data: formulario,
				      type:'post',
				      processData: false,
				      contentType: false,
				      beforeSend:function(a){
				      	$(".preloader").show();
				      },
				      complete:function(a){
				      	$(".preloader").hide();
				      },
				      error:function (data) {

				      	$(".preloader").hide();
				      	swal("Error","Por favor comuniquese con el administrador",'error');

				      },
				      success:function(salida){

                          if(salida.tipo === 'ok'){

                              alerta("Completado","Usuario Registrado","success",
                                  "btn-success",
                                  "Cerrar",function (isConfirm) {

                                      location.reload();

                                  });

                          }
                          else{
                            swal({
									title: "Error", 
									text: salida.mensaje,  
									html: true,
									type: "warning"
								});
                          }
				      }
				    });


				break;

				case 'editar':

					formulario.append('usuario_actual', usuario_actual);
					formulario.append('perfil_actual', perfil_actual);

					$.ajax({
				      url:'usuario_editar',
				      data: formulario,
				      type:'post',
				      processData: false,
				      contentType: false,
				      beforeSend:function(a){
				      	$(".preloader").show();
				      },
				      complete:function(a){
				      	$(".preloader").hide();
				      },
				      error:function (data) {

				      	$(".preloader").hide();
				      	swal("Error","Por favor comuniquese con el administrador",'error');

				      },
				      success:function(salida){

				      	if(salida.tipo == 'ok'){

				      		alerta("Completado","Usuario Actualizado","success");

				        }
				        else{
				        	swal({
									title: "Error", 
									text: salida.mensaje,  
									html: true,
									type: "warning"
								});
				        }
				      }
				    });

				break;

				default:
				break;

			}

		});





		$("body").on("click",".btn-asignar-usuario",function(){

			usuario_actual = $(this).parent().parent().children().eq(1).text();

			$.ajax({

				url:'usuario_perfiles_asignados',
				data: {
					usuario_actual:usuario_actual
				},
				type:'post',
				beforeSend:function(a){
				  	$(".preloader").show();
				},
				complete:function(a){
				    $(".preloader").hide();
				},
				error:function (data) {

				    $(".preloader").hide();
				    swal("Error","Por favor comuniquese con el administrador",'error');

				},
				success:function(salida){
					
					var elementos = salida.length;

					if( elementos > 0 ){

						$.each(salida,function(key,valor){

							$("#check_"+valor.perfil_id).attr("checked","checked");


						});


					}

					$("#modal-asignar").modal();
				    
				}

			});

	
		});


		$("#con-asignar-usuario").click(function(){

			var perfiles_elegidos = [];

			$(".elemento_check").each(function(key,valor){

				if( $(this).is(':checked') ){

					perfiles_elegidos.push( $(this).val() );

				}		


			});

			if( perfiles_elegidos.length == 0 ){

				swal("Error","Elija un perfil por lo menos","error");
				return;

			}

			$.ajax({
				url:'usuario_asignar_perfil',
				data: {
					usuario_actual:usuario_actual,
					perfiles_elegidos:perfiles_elegidos
				},
				type:'post',
				beforeSend:function(a){
				      	$(".preloader").show();
				},
				complete:function(a){
				      	$(".preloader").hide();
				},
				error:function (data) {

				    $(".preloader").hide();
				      	swal("Error","Por favor comuniquese con el administrador",'error');

				},
				success:function(salida){
					console.log(salida);
				    if(salida.tipo == 'ok'){

				      	alerta("Completado","Perfiles Asignados Correctamente","success");

				    }
				    else{
				        swal("Error",salida.mensaje,"warning")
				    }
				}


			});

		});



		//Funciones Generales
		
		$("#username-usuario").on("keyup",function(){

			if(!validar_email($(this).val())){

				$(this).parent().addClass('has-error');

			}
			else{
				$(this).parent().removeClass('has-error');
			}

		});



		function desbloquear(){

			$("#modal-usuario input,select").val('').attr("disabled",false).parent().parent().show();

			$("#modal-usuario .box-footer").show();

		}


		function validar_contra(){

			var pass1 = $("#password-usuario").val();
			var pass2 = $("#password-usuario-validar").val();

			if( pass1 == pass2 ){

				return true;

			}

			else{

				
				return false;

			}

		}

		$("body").on("keyup change","#password-usuario-validar,#password-usuario",function(){

			$("#password-usuario-validar").removeClass("has-danger");

			if( !validar_contra() ){

				$("#password-usuario-validar").addClass("has-danger");

			}

		});


	
	});

</script>
@endsection
