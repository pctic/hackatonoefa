@extends('layouts.body_master')

@include("usuario.index.head")

@include("usuario.index.script")

@section("titulo","Usuarios Registrados")

@section("boton")

<button id="btn-crear-usuario" type="button" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Crear </button>

@endsection

@section('content')

<section class="content">
  <div class="row">
    
    <div class="col-md-12">
      <!-- Input addon -->
      <div class="card ">

        <div class="card-body">


          <div class="clearfix"></div>

          <span class="pull-right"></span>

            <table id="tabla_usuarios" class="tablesaw table-bordered table-hover table display nowrap" data-tablesaw-mode="stack"  data-tablesaw-mode="columntoggle">
              <thead>
                <tr>
                  <th>#</th>
                  <th hidden>usuario_id</th>
                  {{-- <th hidden>perfil_id</th> --}}
                  <th>Correo</th>
                  <th>Nombres y Apellidos</th>
                  <th>Perfiles</th>
                  <th>Estado</th>
                  <th>Opciones</th>
                  
                </tr>
              </thead>
              <tbody class="cuerpo_sede">

                @foreach ($usuarios as $key => $u)
                

                  <tr>
                    <td>{{$key+1}}</td>
                    <td hidden>{{$u->usuario_id}}</td>
                    <td>{{$u->persona_correo}}</td>
                    <td>{{ ucfirst( $u->persona_nombre )  }} {{ ucfirst( $u->persona_apellido ) }}</td>
                    <td>

                      
                      
                        <span class="label @switch($u->perfil_id)
                          
                          @case(1) label-success @break

                          @case(2) label-info @break

                          @case(3) label-warning @break

                          @case(4) label-danger @break
                          
                          @case(5) label-success @break

                          @case(6) label-info @break

                          @case(7) label-warning @break

                          @default label-danger @endswitch ">  
                          
                          {{ $u->perfil_nombre }}</span>
                          
                      

                    </td>
                    
                    <td>

                      @if ($u->estado == 1)
                        <button class="btn btn-xs btn-success">Activo</button>
                      @elseif($u->estado == 0)
                         <button class=" btn btn-xs btn-danger">Inactivo</button>
                      @endif

                    </td>
                    <td>
                      


                      <button title="Editar" class="btn-editar-usuario btn btn-xs btn-warning"><i class="fas fa-pencil-alt"></i></button>

                      {{-- <button title="Asignar Perfiles" class="btn-asignar-usuario btn btn-xs btn-primary"><i class="fas fa-users"></i></button> --}}

                        @if ($u->estado == 1)

                          <button title="Dar Baja " class="btn-baja-usuario btn btn-xs btn-danger"><i class="fas fa-arrow-circle-down"></i></button>
                      
                        @elseif($u->estado == 0)

                          <button title="Dar Alta" class="btn-alta-usuario btn btn-xs btn-success"><i class="fas fa-arrow-circle-up"></i></button>

                        @endif


                    </td>

                  </tr>

                  

                @endforeach
                
              </tbody>

            </table>
          </div>


          <!-- /input-group -->
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>

  </div>
</div>
</section>


<!--MODALS-->

@include("usuario.elementos.modal_registrar")

{{-- @include("usuario.elementos.modal_asignar") --}}

<!-- MODALS -->

@endsection

