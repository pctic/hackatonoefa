@section('script')

    <!--morris JavaScript -->
    {{--
    <script src="assets/node_modules/raphael/raphael-min.js"></script> --}} {{--
<script src="assets/node_modules/morrisjs/morris.min.js"></script> --}}
    <script src="assets/node_modules/jquery-sparkline/jquery.sparkline.min.js"></script>

    <!-- Popup message jquery -->
    <script src="assets/node_modules/toast-master/js/jquery.toast.js"></script>

    <!-- Chart JS -->
    <script src="assets/node_modules/echarts/echarts-all.js"></script>
    {{--
    <script src="assets/node_modules/echarts/echarts-init.js"></script> --}}
    <!-- Popup message jquery -->
    <script src="assets/node_modules/select2/dist/js/select2.min.js"></script>

    <script>


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(function () {

            //Iniciamos select2 ajax

            $("#recinto").select2();

            $("#actividad").select2();

            $("#venue").select2();

            //Al cambiar de recinto cambiamos los venue

            $('#recinto').on('select2:select', function (e) {

                var data = e.params.data;
                var recinto_id = data.id;
                console.log(data);

                var options = [];

                $.ajax({
                    url: 'listar_recinto_venue',
                    data: {
                        recinto_id: recinto_id
                    },
                    type: 'post',
                    beforeSend: function (a) {
                        $(".preloader").show();
                    },
                    complete: function (a) {
                        $(".preloader").hide();
                    },
                    error: function (data) {
                        $(".preloader").hide();
                        swal("Error", "Por favor comuniquese con el administrador", 'error');
                    },
                    success: function (salida) {

                        console.log(salida);

                        $.each(salida, function (key, data) {

                            options.push({
                                text: data.venue_nombre,
                                id: data.venue_id
                            });


                        });

                        $("#venue").empty().select2({
                            data: options
                        });


                    }
                });


            });


        });


    </script>
@endsection
