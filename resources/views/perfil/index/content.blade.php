@extends('layouts.body_master')
    @include('perfil.index.head') 
@section("titulo","Perfil") 
@section('content')

<div class="row">
    <!-- Column -->
    <div class="col-lg-3 col-xlg-3 col-md-5">
        <div class="card">
            <div class="card-body">
                <center class="m-t-30">

                    <input type="file" id="foto_persona" name="foto_persona" class="dropify" data-height="200" data-width="200" @if( session(
                        "foto_url") !="" ) data-default-file='{{ "../storage/app/" . session("foto_url")  }}' @else data-default-file="assets/images/users/1.jpg"
                        @endif />

                    <h4 class="card-title m-t-10">{{session("persona_nombre")}}</h4>
                    <h6 class="card-subtitle">{{session("perfil_nombre")}}</h6>

                </center>
            </div>
            <div>
                <hr> </div>
            <div class="card-body">

                <small class="text-muted">Correo </small>
                <h6>{{$dat['persona_correo']}}</h6>

                <small class="text-muted p-t-30 db">DNI</small>
                <h6>{{$dat['persona_documento']}}</h6>


            </div>
        </div>
    </div>
    <!-- Column -->
    <!-- Column -->
    <div class="col-lg-9 col-xlg-9 col-md-7">
        <div class="card">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs profile-tab" role="tablist">

                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#settings" role="tab">Información Personal</a> </li>

            </ul>
            <!-- Tab panes -->
            <div class="tab-content">

                <div class="tab-pane active" id="settings" role="tabpanel">
                    <div class="card-body">

                        <form class="form-horizontal form-material" id="form_perfil">

                            {{ csrf_field() }}

                            <div class="row">

                                <div class="col-6">

                                    <div class="form-group">
                                        <label class="col-md-12">Nombre</label>
                                        <div class="col-md-12">
                                            <input type="text" name="nombre-persona" id="nombre-persona" class="form-control form-control-line" value="{{$dat['persona_nombre']}}">
                                        </div>
                                    </div>

                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="example-email" class="col-md-12">Apellido</label>
                                        <div class="col-md-12">
                                            <input type="text" name="apellido-persona" id="apellido-persona" class="form-control form-control-line" value="{{$dat['persona_apellido']}}">
                                        </div>
                                    </div>

                                </div>

                            </div>


                            <div class="row">

                                <div class="col-6">

                                    <div class="form-group">
                                        <label class="col-md-12">Correo</label>
                                        <div class="col-md-12">
                                            <input type="email" name="correo-persona" id="correo-persona" class="form-control form-control-line" value="{{$dat['persona_correo']}}">
                                        </div>
                                    </div>

                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="example-email" class="col-md-12">Teléfono</label>
                                        <div class="col-md-12">
                                            <input type="text" name="telefono-persona" id="telefono-persona" class="form-control form-control-line" value="{{$dat['persona_telefono']}}">
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-6">

                                    <div class="form-group">
                                        <label for="example-email" class="col-md-12">Contraseña</label>
                                        <div class="col-md-12">
                                            <input type="password" name="contra-persona" id="contra-persona" class="form-control form-control-line">
                                        </div>
                                    </div>

                                </div>

                                <div class="col-6">

                                    <div class="form-group">
                                        <label for="example-email" class="col-md-12">Repite Contraseña</label>
                                        <div class="col-md-12">
                                            <input type="password" name="contra-persona-validar" id="contra-persona-validar" class="form-control form-control-line">
                                        </div>
                                    </div>

                                </div>


                            </div>

                            <div class="row">

                                <div class="col-6">

                                    <div class="form-group">
                                        <label class="col-md-12">Dirección</label>
                                        <div class="col-md-12">
                                            <input type="text" name="direccion-persona" id="direccion-persona" class="form-control form-control-line" value="{{$dat['persona_direccion']}}">
                                        </div>
                                    </div>

                                </div>

                                <div class="col-6">

                                    <div class="form-group">
                                        <label class="col-md-12"> Perfil </label>
                                        <div class="col-md-12">
                                            <input class="form-control form-control-line" value="{{session("perfil_nombre")}}" disabled>

                                        </div>
                                    </div>

                                </div>


                            </div>



                        </form>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <button class="btn btn-info" id="actualizar_perfil"> <i class="fas fa-pencil-alt"></i> Actualizar Perfil</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Column -->
</div>
@endsection

    @include("perfil.index.script")