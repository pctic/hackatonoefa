@section('head')

    <style>

        #tabla td {
            padding-left: 5px;
        }

        img {
            margin-bottom: 10px;
            
        }

        .file {
            background: white;
        }

        .profilepicture {
            height: 120px;
            width: 120px;
            background: url('http://41.76.211.172/com.tailorapp.spring/resources/img/no-profile-img.jpg');
            border-radius: 50%;
            background-size: 100%;
            margin-left: auto;
            margin-right: auto;
        }
        .dropify-wrapper .dropify-preview .dropify-render img{
            height:200px !important;
            width:200px !important;
            border-radius: 50%;
        }
        .dropify-wrapper{
            border:none !important;
        }
        .dropify-filename{
            display:none;
        }
        .dropify-wrapper .dropify-preview .dropify-infos .dropify-infos-inner p.dropify-infos-message::before{
            display:none;
        }

        .dropify-wrapper .dropify-preview .dropify-render img{
            height: 150px !important;
            width: 150px !important;
            max-width: 150px !important;
            max-height: 150px !important;
        }

        .contener_imagen_perfil{
            padding-top:0px;
            padding-bottom:0px;
        }

        @media (max-width: 768px){

            .container-fluid{
                padding:5px;
            }
            .dropify-wrapper.touch-fallback .dropify-preview .dropify-render img{
                height:150px !important;
                width:150px !important;
            }
            .profile-tab li a.nav-link, .customtab li a.nav-link{
                padding:10px;
            }
            .topbar ul.dropdown-user li .dw-user-box .u-img img{
                width:100% !important;
                height:auto !important;
            }
            .topbar ul.dropdown-user{
                width:100% !important;
            }
            .dropify-clear{
                display:none !important;
            }
            .dropify-infos{
                display:none;
                /* margin-top:20px !important; */
            }
            .contener_imagen_perfil{
                padding:0px;
            }
            .contenedor_informacion_principal{
                padding-top:0px;
            }

            .dropify-wrapper .dropify-preview .dropify-render img{
                height: 150px !important;
                width: 150px !important;
                max-width: 150px !important;
                max-height: 150px !important;
            }


        }


    </style>
    <link rel="stylesheet" href="assets/node_modules/dropify/dist/css/dropify.min.css">   

@endsection
