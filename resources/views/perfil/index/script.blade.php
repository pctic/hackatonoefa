@section('script')
<script src="assets/node_modules/dropify/dist/js/dropify.min.js"></script>
<script>
    $(function () {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.dropify').dropify({

                messages: {
                    default: 'Elija una imagen',
                    replace: 'Reemplazar Imagen',
                    remove: 'Eliminar',
                    error: 'Ha surguido un error, favor de recargar la página e intentar nuevamente'
                }

            });

            $("#actualizar_perfil").click(function(){

                var nombre = $("#persona-nombre").val();
                var apellido = $("#persona-apellido").val();
                var correo = $("#persona-correo").val();
                var telefono = $("#persona-telefono").val();
                var contra = $("#persona-contra").val();
                var direccion = $("#persona-direccion").val();


                if( !validar_contra() && contra != "" ){
				
                    swal("Error","Las contraseñas no coinciden","warning");
                    return;

                }

                var input = document.querySelector('input[type=file]'),
                foto_persona = input.files[0];

                if( nombre == '' || apellido == '' || correo == '' ){

                    swal("Error","Favor de completar todos los datos","warning");

                }

                var formData = new FormData($("#form_perfil")[0]);

                formData.append("foto_persona", foto_persona);

                $.ajax({
                    url: 'actualizar_perfil',
                    type: 'post',
                    data: formData,
                    processData: false,
                    contentType: false,
                    beforeSend:function(a){
                        $(".preloader").show();
                    },
                    complete:function(a){
                        $(".preloader").hide();
                    },
                    error:function (data) {

                        $(".preloader").hide();
                        swal("Error","Por favor comuniquese con el administrador",'error');

                    },
                    success: function (salida) {
                        console.log(salida);

                        if(salida.tipo === 'ok'){

                            alerta("Completado","Perfil Actualizado","success",
                                 "btn-success",
                                 "Cerrar",function (isConfirm) {

                                      location.reload();

                                });

                        }
                        else{

                            swal({
                                title: "Atención", 
                                text: ""+salida.mensaje+"",  
                                html:true,
                                type: 'warning',
                            });
                        }


                    }
			    });



            });

            
            $("body").on("keyup change","#password-usuario-validar,#password-usuario",function(){

                $("#password-usuario-validar").removeClass("has-danger");

                if( !validar_contra() ){

                    $("#password-usuario-validar").addClass("has-danger");

                }

            });

            function validar_contra(){

                var pass1 = $("#contra-persona").val();
                var pass2 = $("#contra-persona-validar").val();

                if( pass1 == pass2 ){

                    return true;

                }

                else{

                    
                    return false;

                }

            }



        });
</script>
@endsection
