<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::resource('emergencias', 'emergenciasController');

Route::post('emergencias_update/{id}', 'emergenciasController@emergencias_update');

Route::resource('registro_mc', 'registro_mcController');

Route::post('registro_mc_update/{id}', 'registro_mcController@registro_mc_update');

