<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get("perfil_admin",'usuarioController@perfil_admin')->name("perfil_admin");



//Login y Registro

Route::get('/','Auth\LoginController@index')->name("vista_login");

Route::post('login','Auth\LoginController@login')->name('login');

Route::post('forgot_password','Auth\ForgotPasswordController@index');

Route::get('logout','Auth\LoginController@logout')->name('logout');

Route::post('registrar_usuario','Auth\RegisterController@validator');


//Dashboard

Route::get('dashboard','dashboardController@index')->name('dashboard');


//Usuarios

Route::get('usuario', 'usuarioController@index')->name('vista_usuario');
Route::get('usuario_activo', 'usuarioController@usuario_activo')->name('vista_usuario_activo');

Route::post('usuario_crear','usuarioController@usuario_crear');

Route::post('usuario_ver_datos','usuarioController@usuario_ver_datos');

Route::post('usuario_editar','usuarioController@usuario_editar');

Route::post('usuario_estado','usuarioController@usuario_estado');

Route::post('usuario_perfiles_asignados','usuarioController@usuario_perfiles_asignados');

Route::post('usuario_asignar_perfil','usuarioController@usuario_asignar_perfil');

//Perfil

Route::get('perfil','perfilController@index')->name('vista_perfil');
Route::post('actualizar_perfil','perfilController@actualizar_perfil');
Route::post('actualizar_perfil_medico','perfilController@actualizar_perfil_medico');


//Dropzone

Route::get('drops', 'dropsController@index')->name('vista_drops');
Route::post('dropo', 'dropsController@dropo');
Route::post('dropo_proyecto', 'dropsController@dropo_proyecto');
Route::get('descargar/{archivo}',  array('as' => 'archivo', 'uses' => 'dropsController@descargar'));
Route::post('borrar_archivo','dropsController@borrar');

//Formulario

Route::get('formulario','formularioController@index')->name('formulario');
Route::get('emergencias_vista', 'emergenciasController@emergencias_vista')->name('emergencias_vista');
Route::get('emergencias_registro', 'emergenciasController@emergencias_registro')->name('emergencias_registro');    

// Power BI
Route::get('redes','redesController@index')->name('redes');
Route::get('medidas_correctivas','medidasCorrectivasController@index')->name('medidas_correctivas');

Route::get('registro_mc_vista', 'registro_mcController@registro_mc_vista')->name('registro_mc_vista');    


