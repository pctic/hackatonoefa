$(".tab-wizard").steps({
    headerTag: "h6",
    bodyTag: "section",
    transitionEffect: "fade",
    titleTemplate: '<span class="step">#index#</span> #title#',
    labels: {
        finish: "Finalizar",
        next: "Siguiente",
        previous: "Anterior",
    },

    onFinished: function (event, currentIndex) {

        swal({
            title: "Registrar Consulta",
            text: "¿Está seguro que quiere registrar la consulta? Una vez cierre la consulta, esta será registrada y almacenada en la historia clínica del paciente.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#009efb",
            confirmButtonText: "Si, registrar!",
            closeOnConfirm: false
        }, function () {

            var objeto_consulta = {};

            //Variables globales
            var consulta_id = 1;
z
            //VITALES
            //objeto  funciones vitales
            var obj_funciones_vitales = {};

        @include('consulta.elements.arraygeneral.ingreso')

            objeto_consulta.funciones_vitales = obj_funciones_vitales;

            //Objeto Examen Fisico
            var array_examen_fisico = [];

        @include('consulta.elements.examen_fisico')
            objeto_consulta.examen_fisico = array_examen_fisico;


            //DIAGNOSTICO
            var array_diagnostico = [];
            $(".diagnostico").each(function (key, valor) {

                var diagnostico_id = null;
                var diagnostico_nombre = $(this).find(".campo_diagnostico").val();

                var obj_tmp_diagnostico = {
                    diagnostico_id: diagnostico_id,
                    diagnostico_nombre: diagnostico_nombre,
                    consulta_id: consulta_id
                }
                array_diagnostico.push(obj_tmp_diagnostico);
            });

            objeto_consulta.diagnostico = array_diagnostico;


            //PLAN TRABAJO
            var objeto_plan_trabajo = {};
            var array_examen_consulta = [];

            var trabajo_comentario = $("#comentario_plan_trabajo").val();
            $(".examen").each(function (key, valor) {

                var plan_trabajo_id = null;
                var examen_consulta_id = null;
                var examen_consulta_nombre = $(this).find(".campo_examen").val();

                var obj_tmp_consulta = {
                    plan_trabajo_id: plan_trabajo_id,
                    examen_consulta_id: examen_consulta_id,
                    examen_consulta_nombre: examen_consulta_nombre
                }
                array_examen_consulta.push(obj_tmp_consulta);
            });
            objeto_plan_trabajo.examen_consulta = array_examen_consulta;
            objeto_plan_trabajo.plan_trabajo_comentario = trabajo_comentario;
            objeto_plan_trabajo.consulta_id = consulta_id;

            objeto_consulta.plan_trabajo = objeto_plan_trabajo;

            //RECETA
            var objeto_receta = {};
            var array_medicamento = [];

            var receta_id = null;
            var medicamento_id = null;
            var medicamento_nombre = null;
            var medicamento_tiempo = null;
            var medicamento_periodicidad = null;
            var medicamento_cantidad = null;
            var medicamento_unidades_id = null;
            var medicamento_unidades_nombre = null;

            var receta_comentario = $("#comentario_receta").val();
            $(".receta_elemento").each(function (key, valor) {

                receta_id = null;
                medicamento_id = null;
                medicamento_nombre = $(this).find(".campo_medicamento").val();
                medicamento_tiempo = $(this).find(".campo_tiempo").val();
                medicamento_periodicidad = $(this).find(".campo_periodicidad").val();
                medicamento_cantidad = $(this).find(".campo_cantidad").val();
                medicamento_unidades_id = null;
                medicamento_unidades_nombre = "mg";

                var obj_tmp_medicamento = {
                    receta_id: receta_id,
                    medicamento_id: medicamento_id,
                    medicamento_nombre: medicamento_nombre,
                    medicamento_tiempo: medicamento_tiempo,
                    medicamento_periodicidad: medicamento_periodicidad,
                    medicamento_cantidad: medicamento_cantidad,
                    medicamento_unidades_id: medicamento_unidades_id,
                    medicamento_unidades_nombre: medicamento_unidades_nombre
                }
                array_medicamento.push(obj_tmp_medicamento);
            });
            objeto_receta.medicamento = array_medicamento;
            objeto_receta.receta_comentario = receta_comentario;
            objeto_receta.consulta_id = consulta_id;


            objeto_consulta.receta = objeto_receta;


            //logs
            console.log(objeto_consulta);


            swal("Registrado!", "Se ha registrado la consulta.", "success");


        });

    }
})
;


var form = $(".validation-wizard").show();

$(".validation-wizard").steps({
    headerTag: "h6",
    bodyTag: "section",
    transitionEffect: "fade",
    titleTemplate: '<span class="step">#index#</span> #title#',
    labels: {
        finish: "Finalizar"
    },
    onStepChanging: function (event, currentIndex, newIndex) {
        return currentIndex > newIndex || !(3 === newIndex && Number($("#age-2").val()) < 18) && (currentIndex < newIndex && (form.find(".body:eq(" + newIndex + ") label.error").remove(), form.find(".body:eq(" + newIndex + ") .error").removeClass("error")), form.validate().settings.ignore = ":disabled,:hidden", form.valid())
    },
    onFinishing: function (event, currentIndex) {
        return form.validate().settings.ignore = ":disabled", form.valid()
    },
    onFinished: function (event, currentIndex) {

        swal({
            title: "Registrar Consulta",
            text: "¿Está seguro que quiere registrar la consulta? Una vez cierre la consulta, esta será registrada y almacenada en la historia clínica del paciente.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, registrar!",
            closeOnConfirm: false
        }, function () {
            swal("Deleted!", "Your imaginary file has been deleted.", "success");
        });

        // swal("Consula Registrada!", "¿Está seguro que quiere registrar la consulta? Una vez cierre la consulta, esta será registrada y almacenada en la historia clínica del paciente.", "success");
    }
}), $(".validation-wizard").validate({
    ignore: "input[type=hidden]",
    errorClass: "text-danger",
    successClass: "text-success",
    highlight: function (element, errorClass) {
        $(element).removeClass(errorClass)
    },
    unhighlight: function (element, errorClass) {
        $(element).removeClass(errorClass)
    },
    errorPlacement: function (error, element) {
        error.insertAfter(element)
    },
    rules: {
        email: {
            email: !0
        }
    }
})
