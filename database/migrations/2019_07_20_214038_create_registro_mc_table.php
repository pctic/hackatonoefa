<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistroMcTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registro_mc', function (Blueprint $table) {
            $table->bigIncrements('registro_mc_id');
            $table->integer("numero_mc")->nullable();
$table->integer("monto_sancion")->nullable();
$table->string("resolucion")->nullable();
$table->string("expediente")->nullable();
$table->string("administrado")->nullable();
$table->string("ubicacion")->nullable();
$table->string("sector")->nullable();
$table->string("materia")->nullable();
$table->string("medidas_correctivas")->nullable();
$table->string("fecha_limite")->nullable();
$table->integer("registro_mc_estado")->nullable();
$table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registro_mc');
    }
}
