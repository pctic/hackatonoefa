<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmergenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emergencias', function (Blueprint $table) {
            $table->bigIncrements('emergencias_id');
            $table->string("razon_social")->nullable();
            $table->string("actividad")->nullable();
            $table->string("domicilio_legal")->nullable();
            $table->string("subsector")->nullable();
            $table->string("distrito")->nullable();
            $table->string("provincia")->nullable();
            $table->string("persona1")->nullable();
            $table->string("persona2")->nullable();
            $table->string("correo1")->nullable();
            $table->string("correo2")->nullable();
            $table->string("telefono1")->nullable();
            $table->string("telefono2")->nullable();
            $table->string("nombre_instalacion")->nullable();
            $table->string("hora_inicio")->nullable();
            $table->string("hora_termino")->nullable();
            $table->integer("area_afectada")->nullable();
            $table->integer("cantidad_derramada")->nullable();
            $table->string("unidad_derramada")->nullable();
            $table->integer("emergencias_estado")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emergencias');
    }
}
