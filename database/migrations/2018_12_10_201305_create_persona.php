<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersona extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        schema::create('persona', function (Blueprint $table) {
            $table->increments('persona_id');
            $table->integer('usuario_id');
            $table->integer('documento_id');//1: dni, 2: ruc
            $table->string('persona_nombre');
            $table->string('persona_apellido');
            $table->string('persona_telefono')->nullable();
            $table->string('persona_correo');
            $table->string('persona_documento');
            $table->string('persona_direccion')->nullable();
            $table->integer('archivo_id')->nullable();//foto

            $table->timestamps();

        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('persona');
    }
}
