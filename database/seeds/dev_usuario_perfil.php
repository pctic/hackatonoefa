<?php

use Illuminate\Database\Seeder;

class dev_usuario_perfil extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Usuario admin
        DB::table('usuario_perfil')->insert(
            array(
                'usuario_id' => 1,
                'perfil_id' => 1,
                'usuario_asignador' => 1,
            ));


    }
}
