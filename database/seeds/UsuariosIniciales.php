<?php

use Illuminate\Database\Seeder;

class UsuariosIniciales extends Seeder
{

    public function run()
    {

        $date = new DateTime("+30 Days");
        $now = date('Y-m-d h:i:s');


        //Usuario 1
        DB::table('usuario')->insert(
            array(
                'name' => 'admin',
                'password' => '$2y$10$zESdiGWxQC6A2GF7aS2VXu8tM1v7lVDB.fROSy0zTyXeWeo2M0GKe',
                'estado' => 1,
                'fecha_expiracion' => $date->format("Y-m-d h:i:s"),
                'intentos' => 0

            )
        );


        //Persona 1
        DB::table('persona')->insert(
            array(
                'usuario_id' => 1,
                'documento_id' => 1,
                'persona_nombre' => 'nombre admin',
                'persona_apellido' => 'apellido admin',
                'persona_correo' => 'admin@admin.com',
                'persona_documento' => '12345679',
                'persona_direccion' => 'Direcion 123 abc ',
                'persona_telefono' => '998564252',
                'created_at' => $now,
                'updated_at' => $now
            )
        );


    }
}
