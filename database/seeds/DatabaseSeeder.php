<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsuariosIniciales::class);
        $this->call(dev_perfil::class);
        $this->call(dev_usuario_perfil::class);


    }
}
