<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers;

class cronBloqueo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'liberar:bloqueo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Se libera el bloqueo luego de 24 horas';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        app()->call(Controllers\cronController::class.'@liberar_bloqueo');
    }
}
