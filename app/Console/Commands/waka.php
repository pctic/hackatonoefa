<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use File;
use Artisan;

class waka extends Command
{

    protected $signature = 'waka:crud {name} {--int=*} {--str=*}';

    protected $description = 'Crea CRUDs con sus MVCs';

    public function __construct()
    {
        parent::__construct();
    }



    public function handle()
    {
        $name = $this->argument('name');

        $campo_int = $this->option('int');

        $campo_str = $this->option('str');

        $texto_migration = "";

        if( count( $campo_str ) == 0 ){

            echo "Es necesario por lo menos un campo string para mostrarlo en la vista del CRUD";
            return;

        }
        
        // // //Creacion del controlador
        $this->controller($name);
        echo "Controlador creado\n";
        // // //Creacion del modelo
        $this->model($name,$campo_int,$campo_str);
        echo "Modelo creado\n";
        // // //Creacion de los request
        $this->request($name);
        echo "Request creado\n";
        //Creacion de la vista
        $this->view($name,$campo_str,$campo_int);
        echo "Vista creada\n";

        // Creamos la ruta en el api.php
        File::append(base_path('routes/api.php'), 'Route::resource(\'' . strtolower($name) . "', '{$name}Controller');\n\n");
        File::append(base_path('routes/api.php'), 'Route::post(\'' . strtolower($name) . "_update/{id}', '{$name}Controller@" .  $name ."_update');\n\n");
        File::append(base_path('routes/web.php'), 'Route::get(\'' . strtolower($name). "_vista" . "', '{$name}Controller@{$name}_vista');    \n\n");
        echo "Ruta creada\n";

        //Ruta del directorio de los migrations
        $path_migration = base_path( 'database/migrations' );

        //Ruta del archivo en el migration
        $path_migration_archivo =  $path_migration . "/*_create_{$name}_table.php";

        //Borramos el migration en caso exista
        foreach( glob( $path_migration ."/*_create_{$name}_table.php" ) as $f ) {

                echo "Eliminando --- " . $f . "\n";
                unlink($f);
                echo "Eliminado --- " . $f . "\n";

        }

        //Ejecutamos el migration
        echo "Creando Migration\n";
        
        Artisan::call('make:migration create_' . strtolower($name) . '_table --create' );


        //Generamos un string con todas las columnas del migration, primero iteramos los int y luego los str
        foreach ($campo_int as $key => $value) {

            $texto_migration .= '$table->integer("' . $value . '")->nullable();'. "\r\n";

        }

        foreach ($campo_str as $key => $value) {

            $texto_migration .= '$table->string("' . $value . '")->nullable();'. "\r\n";

        }

        $texto_migration .= '$table->integer("'. $name .'_estado' . '")->nullable();' . "\r\n";
        $texto_migration .= '$table->timestamps();'. "\r\n";



        //Generamos un array de todos los archivos que coinciden con el nombre del wildcard y elegimos el primero
        $file_contents = file_get_contents( glob($path_migration_archivo)[0] );

        //Intercambiammos 
        $migrationTemplate = str_replace(
            [
                '$table->timestamps();',
                '$table->bigIncrements(' . "'" . 'id' . "'" . ');'
            
            ],
            [
                $texto_migration,
                '$table->bigIncrements(' . "'" . $name . '_id' . "'" . ');'
            
            ],
            $file_contents
        );

        //Modificamos el migration para agregar las columnas
        echo "Creando columnas del migration\n";
        file_put_contents( glob($path_migration_archivo)[0] , $migrationTemplate  );

        //Refrescamos el migration en la BD
        // Artisan::call('migrate:fresh');

    }



    protected function getStub($type)
    {
        return file_get_contents(resource_path("stubs/$type.stub"));
    }



    protected function model($name,$campo_int,$campo_str)
    {

        $lista_modelos = "";

        $cantidad_elementos_int = count( $campo_int );
        $cantidad_elementos_str = count( $campo_str );

        foreach ($campo_int as $key => $value) {
            
            $lista_modelos .= "'" . $value . "'";

            if( $cantidad_elementos_int != $key+1 ){

                $lista_modelos .= ",";

            }

        }

        if( $cantidad_elementos_str == 0 ){

            $lista_modelos .= ",";

        }

        foreach ($campo_str as $key => $value) {

            if( $cantidad_elementos_int > 0 && $key == 0){

                $lista_modelos .= ",";

            }
            
            $lista_modelos .= "'" . $value . "'";

            $lista_modelos .= ",";

        }

        $lista_modelos .= "'" . $name . "_estado'";
        

        $modelTemplate = str_replace(
            [
                '{{modelName}}',
                '{{listModel}}'
            ],
            [
                $name,
                $lista_modelos
            ],
            $this->getStub('Model')
        );

        file_put_contents(app_path("/{$name}.php"), $modelTemplate);
    }



    protected function controller($name)
    {
        $controllerTemplate = str_replace(
            [
                '{{modelName}}',
                '{{modelNamePluralLowerCase}}',
                '{{modelNameSingularLowerCase}}'
            ],
            [
                $name,
                strtolower($name),
                strtolower($name)
            ],
            $this->getStub('Controller')
        );

        file_put_contents(app_path("/Http/Controllers/{$name}Controller.php"), $controllerTemplate);
    }



    protected function request($name)
    {
        $requestTemplate = str_replace(
            ['{{modelName}}'],
            [$name],
            $this->getStub('Request')
        );

        if(!file_exists($path = app_path('/Http/Requests')))
            mkdir($path, 0777, true);

        file_put_contents(app_path("/Http/Requests/{$name}Request.php"), $requestTemplate);
    }



    protected function view($name,$campo_str,$campo_int)
    {

        $primerString =  $campo_str[0];

        //Bloque para crear el content
        $contentTemplate = str_replace(
            [
                '{{viewName}}',
                '{{upperViewName}}',
                '{{primerString}}'
            ],
            [
                $name,
                ucfirst($name),
                $primerString
            ],
            $this->getStub('Content')
        );

        //Creamos la carpeta del modulo en la vista
        if(!file_exists($path_modulo = base_path("resources/views/{$name}") ) )
            mkdir($path_modulo, 0775, true);

        //Creamos la carpeta de index del modulo en la vista
        if(!file_exists($path_index = base_path("resources/views/{$name}/index") ) )
            mkdir($path_index, 0775, true);

        //Creamos el documento respecto a la plantilla content
        file_put_contents( $path_index . "/content.blade.php" , $contentTemplate);


        $elementoModal = "";
        $listaVerDatos = "";
        

        foreach ($campo_int as $key => $value) {

            $elementoModal .= 
            '<div class="col-12">
      
                <div class="form-group">
    
                <label for="' . $name . '_' . $value . '">' . ucfirst( $value ) . '</label>
    
                <div class="input-group">
    
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"><i class="fas fa-cube"></i></span>
                    </div>
    
                    <input type="number" class="form-control" id="' . $value . '" name="' . $value . '" >
    
                </div>
    
                </div>
    
            </div>';

            $listaVerDatos .= '$("#' . $value . '").val(salida.mensaje.' . $value . ');' . "\n\n";

        }

        foreach ($campo_str as $key => $value) {

            $elementoModal .= 
            '<div class="col-12">
      
                <div class="form-group">
    
                <label for="' . $value . '">' . ucfirst( $value ) . '</label>
    
                <div class="input-group">
    
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"><i class="fas fa-cubes"></i></span>
                    </div>
    
                    <input type="text" class="form-control" id="' . $value . '" name="' . $value . '" >
    
                </div>
    
                </div>
    
            </div>';

            $listaVerDatos .= '$("#' . $value . '").val(salida.mensaje.' . $value . ');' . "\n\n";

        }

        



        //Bloque para crear el Modal
        $modalTemplate = str_replace(
            [
                '{{viewName}}',
                '{{upperViewName}}',
                '{{primerString}}',
                '{{elementoModal}}'
                
            ],
            [
                $name,
                ucfirst($name),
                $primerString,
                $elementoModal
            ],
            $this->getStub('Modal')
        );

        //Creamos la carpeta de elemento del modulo en la vista
        if(!file_exists($path_elemento = base_path("resources/views/{$name}/elemento") ) )
            mkdir($path_elemento, 0775, true);
            
        //Creamos el documento respecto a la plantilla modal
        file_put_contents( $path_elemento . "/modal.blade.php" , $modalTemplate);








        //Bloque para crear el script
        $scriptTemplate = str_replace(
            [
                '{{viewName}}',
                '{{upperViewName}}',
                '{{listaVerDatos}}'
            ],
            [
                $name,
                ucfirst($name),
                $listaVerDatos
            ],
            $this->getStub('Script')
        );

        //Creamos el documento respecto a la plantilla script
        file_put_contents( $path_index . "/script.blade.php" , $scriptTemplate);




        //Bloque para crear el head
        $headTemplate = str_replace(
            [
                '{{viewName}}',
                '{{upperViewName}}'
            ],
            [
                $name,
                ucfirst($name)
            ],
            $this->getStub('Head')
        );

        //Creamos el documento respecto a la plantilla head
        file_put_contents( $path_index . "/head.blade.php" , $headTemplate);
    }



}