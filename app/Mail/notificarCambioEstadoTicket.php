<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class notificarCambioEstadoTicket extends Mailable
{
    use Queueable, SerializesModels;
    public $datos;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($datos)
    {
        $this->datos = $datos;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
                ->from('noreply@atlas.pe', 'Atlas noreply')
                ->subject( '[' . $this->datos['ticket_id'] . '] Ticket: ' . $this->datos['ticket_nombre'] )
                ->with(['datos', $this->datos])
                ->view('emails.plantilla_cambio_estado_ticket');
    }
}
