<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class persona extends Model

{
    use Notifiable;

    protected $primaryKey = "persona_id";
    protected $table = "persona";


}
