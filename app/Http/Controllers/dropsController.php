<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use View;
use Response;
use App;


class dropsController extends Controller
{

    public function index()
    {

        return view('drops.index');
    }

    public function dropo_proyecto(Request $request)
    {

        $nombre = request("nombre");

        $subir_archivo = request()->file($nombre)->store('archivos');
        $nombre_original = request()->file($nombre)->getClientOriginalName();

        $archivo = new App\archivo;
        $archivo->save();

        $ag = new App\adetalle;
        $ag->archivo_id = $archivo->archivo_id;
        $ag->adetalle_url = $subir_archivo;
        $ag->adetalle_nombre = str_replace(" ", "_", $nombre_original);
        $ag->save();

        return $ag->adetalle_id;
    }

    public function dropo(Request $request)
    {

        $subir_archivo = request()->file("file")->store('archivos');
        $nombre_original = request()->file("file")->getClientOriginalName();


        $ag = new App\adetalle;
        $ag->adetalle_url = $subir_archivo;
        $ag->adetalle_nombre = str_replace(" ", "_", $nombre_original);
        $ag->save();

        return $ag->adetalle_id;
    }

    public function descargar($a)
    {

        $idarchivo = $a;

        $archivo = App\adetalle::where('adetalle_id', $idarchivo)->first();

        /*$headers = 	array(
                    	'Content-Type'=> $we
                    );*/

        //TODO: you have to split the file name from url

        // return 'storage/app/' .$archivo->adetalle_url;
        return response()->download(public_path() . '/../storage/app/' . $archivo->adetalle_url, $archivo->adetalle_nombre);

    }

    public function borrar(Request $request)
    {
        $idadetalle = $request->input('idadetalle');

        $ida = $idadetalle;

        App\adetalle::where('adetalle_id', $ida)->delete();

        return 'ok';

    }


}
