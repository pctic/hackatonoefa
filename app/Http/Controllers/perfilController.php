<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use View;
use Response;
use App;

class perfilController extends Controller
{

    public function __construct()
    {

        $this->middleware('auth');

    }

    public function index()
    {

        // $id_usuario_actual = session('usuario_id');
        $id_persona_actual = session('persona_id');
        $id_perfil_actual = session('perfil_id');

        $dat = App\persona::where('persona_id', $id_persona_actual)
            ->leftjoin("adetalle as ad", "ad.archivo_id", "=", "persona.archivo_id")
            ->first();

        $cargo = App\perfil::where('perfil_id', $id_perfil_actual)
            ->first();

        if ($id_perfil_actual == 1 || session("usuario_id") == 1) {

            $perfiles = App\perfil::get();

        } else {

            $perfiles = App\perfil::where("perfil_id", "!=", 1)
                ->get();


        }


        return view::make('perfil.index.content')
            ->with("dat", $dat)
            ->with("cargo", $cargo)
            ->with("perfiles", $perfiles);


    }


}
