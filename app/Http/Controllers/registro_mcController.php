<?php
    namespace App\Http\Controllers;

    use App\Http\Requests\registro_mcRequest;
    use App\registro_mc;
    use View;

    class registro_mcController extends Controller
    {
        public function index()
        {
            $registro_mc = registro_mc::latest()->get();
            return response()->json($registro_mc);
        }

        public function store(registro_mcRequest $request)
        {
            $registro_mc = registro_mc::create($request->all());

            if( $registro_mc ){

                return $this->crear_objeto("ok","Actualizacion Completada");

            }
            else{

                return $this->crear_objeto("error","Ha surguido un error, inténtelo nuevamente");

            }
            //return response()->json($registro_mc, 201);
        }
        
        public function show($id)
        {
            $registro_mc = registro_mc::findOrFail($id);

            if( $registro_mc ){

                return $this->crear_objeto("ok",$registro_mc);

            }
            else{

                return $this->crear_objeto("error","Ha surguido un error, inténtelo nuevamente");

            }
            //return response()->json($registro_mc);
        }

        public function registro_mc_update(registro_mcRequest $request, $id)
        {
            $registro_mc = registro_mc::findOrFail($id);
            $registro_mc->update($request->all());

            if( $registro_mc ){

                return $this->crear_objeto("ok","Actualizacion Completada");

            }
            else{

                return $this->crear_objeto("error","Ha surguido un error, inténtelo nuevamente");

            }

            //return response()->json($registro_mc, 200);
        }

        public function destroy($id)
        {
            registro_mc::destroy($id);
            return response()->json(null, 204);
        }

        public function registro_mc_vista()
        {
            $lista_registro_mc = registro_mc::get();
            
            return View("registro_mc.index.content")
                        ->with("lista_registro_mc",$lista_registro_mc)
                        ;

        }
    }