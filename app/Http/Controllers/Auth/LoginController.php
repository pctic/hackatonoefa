<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Validator;
use App;
use View;
use PHPMailer;
use Carbon\Carbon;

class LoginController extends Controller
{ 


    public function __construct()
    {

        $this->middleware('guest', ['only' => ['/', 'forgot_password', 'solicitar_correo_password']]);

    }

    public function index()
    {

        $register_session = session("register_session");
        $login_session = session("login_session");

        session()->forget('register_session');
        session()->forget('login_session');

        return view('login.index.content')
            ->with("register_session",$register_session)
            ->with("login_session",$login_session);

    }

    public function login()
    {
        
        session()->forget('login_session');
        session()->forget('register_session');

        // $this->validate(request(),[
        //   'name' => 'required|string',
        //   'password' => 'required|string'

        // ]);
            

        $name = strtolower(request('name'));

        $password = request('password');


        $credentials =  array(
                            "name" => $name, 
                            "password" => $password
                        );

        $contador = App\User::where( "name",$credentials['name'] );


                        // return $contador->first()['intentos'];

        // if( $contador->first()['intentos'] >= 3 && $contador->first()['usuario_id'] != 1 ){

        //     $contador->update(["estado" => 2]);

        // }

        // if( $contador->count() > 0 ){

        //     $contador->increment("intentos");

        // }
        

        if ($name == '' || $password == '') {


            session()->put("login_session", $this->crear_objeto("warning","Complete todos los campos"));

            
            
            return redirect()->route('vista_login');

        }

        


        if (Auth::attempt($credentials)) {


            $id = auth()->user()->usuario_id;


            $datos2 = App\User::join('usuario_perfil as up', 'up.usuario_id', '=', 'usuario.usuario_id')
                ->join('perfil as per', 'per.perfil_id', '=', 'up.perfil_id')
                ->where("usuario.usuario_id", $id)
                ->where("estado",1)
                ->get();

            

            if( count( $datos2 ) > 0  ){
                
                $contador->update(["intentos" => 0]);

                $datos = $datos2[0];

                $now = Carbon::now();

                $date = Carbon::parse( $datos['fecha_expiracion'] );
                // $luego = Carbon::now()->addHours(4);


             /*   if(  $date->diffInHours($now,false) >= 0 ){

                    session()->put("login_session", $this->crear_objeto("danger","Su contraseña ha expirado, favor de pedir a un administrador que reinicie su contraseña"));

                    return redirect()->route('vista_login');

                }*/


                $array = [];

                    foreach ($datos2 as $key => $value) {

                        $array[] =  $value['perfil_id'];

                    }

                session()->put("perfiles", $array );

                session()->put("datos_personales",$datos);


                

                if ($datos['perfil_id'] == 1) {//Administrador

                    $this->generar_sesion($datos->usuario_id);

                    return redirect()
                                ->action('usuarioController@perfil_admin');

                }

                else if ($datos['perfil_id'] == 2) {//Agente N1

                    $this->generar_sesion($datos->usuario_id);
                    
                    return redirect()
                                ->action('usuarioController@perfil_operario');

                }

                else if ($datos['perfil_id'] == 3) {//Agente N2

                    $this->generar_sesion($datos->usuario_id);
                    
                    return redirect()
                                ->action('usuarioController@perfil_supervisor');

                }


            }
            

        }

        



        session()->put("login_session", $this->crear_objeto("warning","No se pudo validar sus datos en el sistema y/o su cuenta se encuentra inactiva") );

        return redirect()->route('vista_login');

    }


    private function generar_sesion($usuario_id)
    {

        $nueva = new App\sesion;

        $nueva->usuario_id = $usuario_id; 
        $nueva->sesion_estado = 1;
                    
        $nueva->save();

        session()->put("sesion_id",$nueva->sesion_id);


    }

    public function logout()
    {

        //Matamos las sesiones pasadas que esten activas
        App\sesion::
        where("sesion_id", session("sesion_id") )
        ->update(["sesion_estado" => 0]);

        // auth()->logout();
        Auth::logout();
        session()->flush();
        header("Cache-Control: no-store, no-cache, must-revalidate");



        return redirect('/');

    }
}
