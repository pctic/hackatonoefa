<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use View;
use App;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function index(){

        session()->forget('login_session');
        session()->forget('success_session');
        
        $correo = request('correo');

        if( $correo == "" ){

            session()->put("login_session", $this->crear_objeto("warning","Ingrese un correo porfavor"));
 
            return redirect()->route('vista_login');

        }

        //Buscamos los datos
        $sol = App\User::join('persona as p','p.usuario_id','=','usuario.usuario_id')
                ->where('p.persona_correo',$correo)
                ->get();


        //Validamos si los datos se encuentran en el sistema
        if( count( $sol ) > 0 ){

            $save = new App\notificacion;

            $save->cn_id = 1;
            $save->perfil_id = 1;
            $save->usuario_id = $sol[0]['usuario_id'];
            $save->notificacion_mensaje = "Solicitud Cambio de Contraseña";
            $save->notificacion_estado = 1;

            $save->save();

            session()->put("login_session", $this->crear_objeto("success","Muchas gracias, Se ha notificado al administrador"));


        }
        else{

            session()->put("login_session", $this->crear_objeto("success","Este correo no se encuentra registrado"));

        }
        
        return redirect()->route('vista_login');

    }
}
