<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App;
use View;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    // protected function validator(array $data)
    protected function validator(Request $request)
    {

        session()->forget('success_session');
        session()->forget('error_session');


        $contra = request("registrar_contra");


        if (strlen($contra) < 6) {

            session()->put("register_session", $this->crear_objeto("warning", "La contraseña debe tener más de 6 dígitos"));

        }

        $credentials = array(
            "nombre" => request("registrar_nombre"),
            "apellido" => request("registrar_apellido"),
            "correo" => strtolower(request("registrar_correo")),
            "dni" => request("registrar_dni"),
            "contra" => request("registrar_contra"),
            "contra_confirmation" => request("registrar_contra_validar")
        );

        $validator = Validator::make($credentials, [
            'nombre' => ['required', 'string', 'max:255'],
            'apellido' => ['required', 'string', 'max:255'],
            'dni' => ['required', 'string', 'max:8'],
            'correo' => ['required', 'string', 'email', 'max:255'],
            'contra' => ['required', 'string', 'min:6', 'confirmed'],
            'contra_confirmation' => ['required']
        ]);


        if ($validator->fails()) {
            return redirect('/')
                ->withErrors($validator)
                ->withInput();
        }

        $creacion = $this->create($credentials);

        $login_session = session("login_session");
        $register_session = session("register_session");

        session()->forget('login_session');
        session()->forget('register_session');

        $especialidades = App\grupo_especialidad::get();
        $examen = App\examen::get();


        return view::make('login.index.content')
            ->with("login_session", $login_session)
            ->with("register_session", $register_session)
            ->with("especialidades", $especialidades)
            ->with("examen", $examen);

    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\User
     */
    protected function create($credentials)
    {

        $buscar = App\User::where("name", $credentials['correo'])
            ->count();

        if ($buscar > 0) {

            session()->put("register_session", $this->crear_objeto("warning", "Este correo ya esta registrado"));
            return false;
        }

        $usuario = new App\User;
        $usuario->name = $credentials['correo'];
        $usuario->password = bcrypt($credentials['contra']);
        $usuario->estado = 1;
        $usuario->save();

        $persona = new App\persona;
        $persona->usuario_id = $usuario['usuario_id'];
        $persona->documento_id = 1;
        $persona->persona_nombre = $credentials['nombre'];
        $persona->persona_apellido = $credentials['apellido'];
        $persona->persona_correo = $credentials['correo'];
        $persona->persona_documento = $credentials['dni'];
        $persona->save();

        $up = new App\usuario_perfil;
        $up->usuario_id = $usuario['usuario_id'];
        $up->perfil_id = 7;
        $up->usuario_asignador = 1;
        $up->save();

        $paciente = new App\paciente;
        $paciente->paciente_fecha_nacimiento = "2000/10/10 08:08:0000";
        $paciente->usuario_id = $usuario['usuario_id'];
        $paciente->familiar_id = "0";
        $paciente->sede_id = "-1";
        $paciente->clinica_id = "-1";
        $paciente->save();

        session()->put("register_session", $this->crear_objeto("success", "Cuenta registrada, favor de logearse"));

        return true;

    }


    protected function validator_medico(Request $request)
    {


        session()->forget('success_session');
        session()->forget('error_session');


        $contra = request("medico_registrar_contra");


        if (strlen($contra) < 6) {


            session()->put("register_session", $this->crear_objeto("warning", "La contraseña debe tener más de 6 dígitos"));


        }
        /*
                Se modificó
                "especialidad" => request("medico_registrar_especialidad")
                por
                "especialidad_grupo" => request("medico_registrar_especialidad_grupo")
        para registrar el grupo de la especialidad, van numeros del 1 al 6
                */

        $datos = array(
            "nombre" => request("medico_registrar_nombre"),
            "apellido" => request("medico_registrar_apellido"),
            "correo" => strtolower(request("medico_registrar_correo")),
            "dni" => request("medico_registrar_dni"),
            "contra" => request("medico_registrar_contra"),
            "contra_confirmation" => request("medico_registrar_contra_validar"),
            "especialidad_grupo" => request("medico_registrar_especialidad_grupo"),
            "colegiatura" => request("medico_registrar_colegiatura"),
            "consultorio_estado" => request("consultorio_estado"),
            "consultorio_direccion" => request("consultorio_registrar_direccion"),
            "consultorio_ruc" => request("consultorio_registrar_ruc"),
            "consultorio_telefono" => request("consultorio_registrar_telefono")
        );


        $validator = Validator::make($datos, [
            'nombre' => ['required', 'string', 'max:255'],
            'apellido' => ['required', 'string', 'max:255'],
            'dni' => ['required', 'string', 'max:8'],
            'correo' => ['required', 'string', 'email', 'max:255'],
            'contra' => ['required', 'string', 'min:6', 'confirmed'],
            'contra_confirmation' => ['required'],
            'especialidad_grupo' => ['required', 'integer'],
            'colegiatura' => ['required']
        ]);

        if ($validator->fails()) {

            return redirect('/')
                ->withErrors($validator)
                ->withInput();
        }

        $creacion = $this->create_medico($datos);

        $login_session = session("login_session");
        $register_session = session("register_session");

        session()->forget('login_session');
        session()->forget('register_session');

        $especialidades = App\grupo_especialidad::get();
        $examen = App\examen::get();

        return view::make('login.index.content')
            ->with("login_session", $login_session)
            ->with("register_session", $register_session)
            ->with("especialidades", $especialidades)
            ->with("examen", $examen);

    }

    protected function validator_laboratorista(Request $request)
    {


        session()->forget('success_session');
        session()->forget('error_session');

        $contra = request("laboratorista_registrar_contra");


        if (strlen($contra) < 6) {

            session()->put("register_session", $this->crear_objeto("warning", "La contraseña debe tener más de 6 dígitos"));

        }
        $datos = array(
            "nombre" => request("laboratorista_registrar_nombre"),
            "apellido" => request("laboratorista_registrar_apellido"),
            "correo" => strtolower(request("laboratorista_registrar_correo")),
            "dni" => request("laboratorista_registrar_dni"),
            "contra" => request("laboratorista_registrar_contra"),
            "contra_confirmation" => request("laboratorista_registrar_contra_validar"),
            "examen" => request("laboratorista_registrar_examen"),
            "colegiatura" => request("laboratorista_registrar_colegiatura"),
        );


        $validator = Validator::make($datos, [
            'nombre' => ['required', 'string', 'max:255'],
            'apellido' => ['required', 'string', 'max:255'],
            'dni' => ['required', 'string', 'max:8'],
            'correo' => ['required', 'string', 'email', 'max:255'],
            'contra' => ['required', 'string', 'min:6', 'confirmed'],
            'contra_confirmation' => ['required'],
            'examen' => ['required', 'integer'],
            'colegiatura' => ['required']
        ]);


        if ($validator->fails()) {


            return redirect('/')
                ->withErrors($validator)
                ->withInput();

        }

        $creacion = $this->create_laboratorista($datos);


        $login_session = session("login_session");
        $register_session = session("register_session");

        session()->forget('login_session');
        session()->forget('register_session');

        $examen = App\examen::get();
        $especialidades = App\grupo_especialidad::get();


        return view::make('login.index.content')
            ->with("login_session", $login_session)
            ->with("register_session", $register_session)
            ->with("especialidades", $especialidades)
            ->with("examen", $examen);

    }

    protected function create_medico($datos)
    {
        $buscar = App\User::where("name", $datos['correo'])
            ->count();

        if ($buscar > 0) {

            session()->put("register_session", $this->crear_objeto("warning", "Este correo ya esta registrado"));
            return false;
        }

        if ($datos['consultorio_estado'] == "true") {


            $validator = Validator::make($datos, [
                'consultorio_direccion' => ['required', 'string', 'max:255'],
                'consultorio_telefono' => ['required', 'string', 'max:255'],
                'consultorio_ruc' => ['required', 'string', 'max:255'],
            ]);

            if ($validator->fails()) {
                session()->put("register_session", $this->crear_objeto("warning", "Debe completar todos los campos de su consultorio"));
                return false;
            }

            $usuario = new App\User;
            $usuario->name = $datos['correo'];
            $usuario->password = bcrypt($datos['contra']);
            $usuario->estado = 1;
            $usuario->save();

            $persona = new App\persona;
            $persona->usuario_id = $usuario['usuario_id'];
            $persona->documento_id = 1;
            $persona->persona_nombre = $datos['nombre'];
            $persona->persona_apellido = $datos['apellido'];
            $persona->persona_correo = $datos['correo'];
            $persona->persona_documento = $datos['dni'];
            $persona->save();

            $up = new App\usuario_perfil;
            $up->usuario_id = $usuario['usuario_id'];
            $up->perfil_id = 2;
            $up->usuario_asignador = 1;
            $up->save();

            $medico = new App\medico;
            $medico->usuario_id = $usuario['usuario_id'];
            $medico->medico_registro_cmp = $datos['colegiatura'];
            $medico->medico_estado = 2;
            $medico->medico_duracion_atencion = 30;
            $medico->save();

            $medico_id = $medico;

            /*            $especialidad_medico = new App\medico_especialidad();
                        $especialidad_medico->medico_id = $medico_id['medico_id'];
                        $especialidad_medico->especialidad_id = $datos['especialidad'];
                        $especialidad_medico->save();
            */


            $especialidad_medico = new App\medico_especialidad();
            $especialidad_medico->medico_id = $medico_id['medico_id'];

            if ($datos['especialidad_grupo'] == 2) {

                $especialidad_medico->especialidad_id = 50;
                
            } elseif ($datos['especialidad_grupo'] == 3) {

                $especialidad_medico->especialidad_id = 51;

            } elseif ($datos['especialidad_grupo'] == 4) {

                $especialidad_medico->especialidad_id = 52;

            } elseif ($datos['especialidad_grupo'] == 5) {

                $especialidad_medico->especialidad_id = 53;

            } elseif ($datos['especialidad_grupo'] == 6) {

                $especialidad_medico->especialidad_id = 54;

            } else {

                $especialidad_medico->especialidad_id = 1;

            }

            $especialidad_medico->save();


            $clinica = new App\clinica;
            $clinica->clinica_nombre = $datos['nombre'];
            $clinica->usuario_id = $usuario->usuario_id;
            $clinica->clinica_direccion = $datos['consultorio_direccion'];
            $clinica->clinica_telefono = $datos['consultorio_telefono'];
            $clinica->clinica_estado = 2;
            $clinica->save();

            $uxp = new App\usuario_perfil;
            $uxp->usuario_id = $usuario->usuario_id;
            $uxp->perfil_id = 9;
            $uxp->usuario_asignador = 1;
            $uxp->save();


            $sede = new App\sede;
            $sede->clinica_id = $clinica->clinica_id;
            $sede->sede_nombre = "Principal";
            $sede->sede_direccion = $datos['consultorio_direccion'];
            $sede->sede_telefono = $datos['consultorio_telefono'];
            $sede->sede_estado = 1;
            $sede->save();


            $sxm = new App\sede_medico;
            $sxm->medico_id = $medico->medico_id;
            $sxm->sede_id = $sede->sede_id;

            if ($datos['especialidad_grupo'] == 2) {

                $sxm->especialidad_id = 50;
                
            } elseif ($datos['especialidad_grupo'] == 3) {

                $sxm->especialidad_id = 51;

            } elseif ($datos['especialidad_grupo'] == 4) {

                $sxm->especialidad_id = 52;

            } elseif ($datos['especialidad_grupo'] == 5) {

                $sxm->especialidad_id = 53;

            } elseif ($datos['especialidad_grupo'] == 6) {

                $sxm->especialidad_id = 54;

            } else {
                
                $sxm->especialidad_id = 1;

            }

            $sxm->save();

            session()->put("register_session", $this->crear_objeto("success", "Cuenta registrada, Un administrador validará sus datos y se pondra en contacto con usted Clinica"));

            return true;

            // return 444;
        } else {

            $usuario = new App\User;
            $usuario->name = $datos['correo'];
            $usuario->password = bcrypt($datos['contra']);
            $usuario->estado = 1;
            $usuario->save();

            $persona = new App\persona;
            $persona->usuario_id = $usuario['usuario_id'];
            $persona->documento_id = 1;
            $persona->persona_nombre = $datos['nombre'];
            $persona->persona_apellido = $datos['apellido'];
            $persona->persona_correo = $datos['correo'];
            $persona->persona_documento = $datos['dni'];
            $persona->save();

            $up = new App\usuario_perfil;
            $up->usuario_id = $usuario['usuario_id'];
            $up->perfil_id = 2;
            $up->usuario_asignador = 1;
            $up->save();

            $medico = new App\medico;
            $medico->usuario_id = $usuario['usuario_id'];
            $medico->medico_registro_cmp = $datos['colegiatura'];
            $medico->medico_estado = 2;
            $medico->medico_duracion_atencion = 30;
            $medico->save();

            $medico_id = $medico;

            $especialidad_medico = new App\medico_especialidad();
            $especialidad_medico->medico_id = $medico_id['medico_id'];
            
            if ($datos['especialidad_grupo'] == 2) {

                $especialidad_medico->especialidad_id = 50;

            } elseif ($datos['especialidad_grupo'] == 3) {

                $especialidad_medico->especialidad_id = 51;

            } elseif ($datos['especialidad_grupo'] == 4) {

                $especialidad_medico->especialidad_id = 52;

            } elseif ($datos['especialidad_grupo'] == 5) {

                $especialidad_medico->especialidad_id = 53;

            } elseif ($datos['especialidad_grupo'] == 6) {

                $especialidad_medico->especialidad_id = 54;

            } else {

                $especialidad_medico->especialidad_id = 1;
                
            }

            $especialidad_medico->save();


            session()->put("register_session", $this->crear_objeto("success", "Cuenta registrada, Un administrador validará sus datos y se pondra en contacto con usted medico"));

            return true;
        }


    }

    protected
    function create_laboratorista($datos)
    {


        $buscar = App\User::where("name", $datos['correo'])
            ->count();


        if ($buscar > 0) {

            session()->put("register_session", $this->crear_objeto("warning", "Este correo ya esta registrado"));
            return false;
        }

        $usuario = new App\User;
        $usuario->name = $datos['correo'];
        $usuario->password = bcrypt($datos['contra']);
        $usuario->estado = 1;
        $usuario->save();


        $persona = new App\persona;
        $persona->usuario_id = $usuario['usuario_id'];
        $persona->documento_id = 1;
        $persona->persona_nombre = $datos['nombre'];
        $persona->persona_apellido = $datos['apellido'];
        $persona->persona_correo = $datos['correo'];
        $persona->persona_documento = $datos['dni'];
        $persona->save();


        $up = new App\usuario_perfil;
        $up->usuario_id = $usuario['usuario_id'];
        $up->perfil_id = 3;
        $up->usuario_asignador = 1;
        $up->save();

        $laboratorista = new App\laboratorista();
        $laboratorista->usuario_id = $usuario['usuario_id'];
        $laboratorista->laboratorista_colegiatura = $datos['colegiatura'];
        $laboratorista->laboratorista_estado = 2;
        $laboratorista->laboratorista_duracion_atencion = 30;
        $laboratorista->save();

        $laboratorista_id = $laboratorista->laboratorista_id($usuario['usuario_id']);


        $laboratorista_exa = new App\laboratorista_examen();
        $laboratorista_exa->laboratorista_id = $laboratorista_id['laboratorista_id'];
        $laboratorista_exa->examen_id = $datos['examen'];
        $laboratorista_exa->save();


        session()->put("register_session", $this->crear_objeto("success", "Cuenta registrada, Un administrador validará sus datos y se pondra en contacto con usted"));

        return true;

    }

    protected
    function validator_clinica(Request $request)
    {

        session()->forget('success_session');
        session()->forget('error_session');


        $contra = request("clinica_registrar_contra");


        if (strlen($contra) < 6) {

            session()->put("register_session", $this->crear_objeto("warning", "La contraseña debe tener más de 6 dígitos"));

        }

        $datos = array(
            "nombre" => request("clinica_registrar_nombre"),
            "telefono" => request("clinica_registrar_telefono"),
            "direccion" => request("clinica_registrar_direccion"),
            "correo" => strtolower(request("clinica_registrar_correo")),
            "ruc" => request("clinica_registrar_ruc"),
            "contra" => request("clinica_registrar_contra"),
            "contra_confirmation" => request("clinica_registrar_contra_validar")
        );

        $validator = Validator::make($datos, [
            'nombre' => ['required', 'string', 'max:255'],
            'telefono' => ['required', 'string', 'max:255'],
            'direccion' => ['required', 'string', 'max:255'],
            'correo' => ['required', 'string', 'email', 'max:255'],
            'ruc' => ['required', 'string', 'max:11'],
            'contra' => ['required', 'string', 'confirmed'],
            'contra_confirmation' => ['required']
        ]);

        if ($validator->fails()) {
            return redirect('/')
                ->withErrors($validator)
                ->withInput();
        }

        $creacion = $this->create_clinica($datos);

        $login_session = session("login_session");
        $register_session = session("register_session");

        session()->forget('login_session');
        session()->forget('register_session');

        $especialidades = App\grupo_especialidad::get();
        $examen = App\examen::get();

        return view::make('login.index.content')
            ->with("login_session", $login_session)
            ->with("register_session", $register_session)
            ->with("especialidades", $especialidades)
            ->with("examen", $examen);

    }


    protected
    function create_clinica($datos)
    {

        $buscar = App\User::where("name", $datos['correo'])
            ->count();

        if ($buscar > 0) {

            session()->put("register_session", $this->crear_objeto("warning", "Este correo ya esta registrado"));
            return false;
        }

        $usuario = new App\User;
        $usuario->name = $datos['correo'];
        $usuario->password = bcrypt($datos['contra']);
        $usuario->estado = 1;
        $usuario->save();

        $persona = new App\persona;
        $persona->usuario_id = $usuario['usuario_id'];
        $persona->documento_id = 2;
        $persona->persona_nombre = $datos['nombre'];
        $persona->persona_apellido = "";
        $persona->persona_correo = $datos['correo'];
        $persona->persona_documento = $datos['ruc'];
        $persona->save();

        $up = new App\usuario_perfil;
        $up->usuario_id = $usuario['usuario_id'];
        $up->perfil_id = 9;
        $up->usuario_asignador = 1;
        $up->save();

        $paciente = new App\clinica;
        $paciente->usuario_id = $usuario['usuario_id'];
        $paciente->clinica_nombre = $datos['nombre'];
        $paciente->clinica_telefono = $datos['telefono'];
        $paciente->clinica_direccion = $datos['direccion'];
        $paciente->clinica_estado = 2;

        $paciente->save();

        session()->put("register_session", $this->crear_objeto("success", "Cuenta registrada, Un administrador validará sus datos y se pondra en contacto con usted"));

        return true;

    }

}
