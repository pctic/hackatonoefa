<?php
    namespace App\Http\Controllers;

    use App\Http\Requests\emergenciasRequest;
    use App\emergencias;
    use View;

    class emergenciasController extends Controller
    {
        public function index()
        {
            $emergencias = emergencias::latest()->get();
            return response()->json($emergencias);
        }

        public function store(emergenciasRequest $request)
        {
            $emergencias = emergencias::create($request->all());

            if( $emergencias ){

                return $this->crear_objeto("ok","Actualizacion Completada");

            }
            else{

                return $this->crear_objeto("error","Ha surguido un error, inténtelo nuevamente");

            }
            //return response()->json($emergencias, 201);
        }
        
        public function show($id)
        {
            $emergencias = emergencias::findOrFail($id);

            if( $emergencias ){

                return $this->crear_objeto("ok",$emergencias);

            }
            else{

                return $this->crear_objeto("error","Ha surguido un error, inténtelo nuevamente");

            }
            //return response()->json($emergencias);
        }

        public function emergencias_update(emergenciasRequest $request, $id)
        {
            $emergencias = emergencias::findOrFail($id);
            $emergencias->update($request->all());

            if( $emergencias ){

                return $this->crear_objeto("ok","Actualizacion Completada");

            }
            else{

                return $this->crear_objeto("error","Ha surguido un error, inténtelo nuevamente");

            }

            //return response()->json($emergencias, 200);
        }

        public function destroy($id)
        {
            emergencias::destroy($id);
            return response()->json(null, 204);
        }

        public function emergencias_vista()
        {
            $lista_emergencias = emergencias::get();
            
            return View("emergencias.index.content")
                        ->with("lista_emergencias",$lista_emergencias)
                        ;

        }

        public function emergencias_registro()
        {
            return View::make('emergencias.index.registro');
        }
    }