<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use View;
use Response;
use App;
use DB;

class medidasCorrectivasController extends Controller
{
    public function index()
    {
        return View::make('medidas_correctivas.index.content');
    }
}
