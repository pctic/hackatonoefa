<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App;

use App\Exports\wakaExport;
use Maatwebsite\Excel\Facades\Excel;

date_default_timezone_set('America/Lima');

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {

    }


    public function crear_objeto($tipo, $mensaje)
    {

        $array = array(
            "tipo" => $tipo,
            "mensaje" => $mensaje
        );

        return $array;

    }

    public function formato_fecha($fecha)
    {

        $year = substr($fecha, 6, 10);
        $dia = substr($fecha, 0, 2);
        $mes = substr($fecha, 3, 2);

        $fecha_iso = $year .= "-" . $mes .= "-" . $dia;

        return $fecha_iso;

    }


    public function excel()
    {

        $plantilla_id = request("plantilla_id");

        return Excel::download(new wakaExport($plantilla_id), 'Plantilla.xlsx');

    }


}
