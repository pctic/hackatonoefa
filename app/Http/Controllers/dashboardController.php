<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use View;
use Response;
use App;
use DB;

class dashboardController extends Controller
{

    public function __construct()
    {

        $this->middleware('auth');

    }


    public function index()
    {
        return View::make('dashboard.admin.content');
    }


}
