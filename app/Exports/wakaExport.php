<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use App;


class wakaExport implements FromView,ShouldAutoSize
{

    public function __construct( $plantilla_id )
    {

        $this->plantilla_id  = $plantilla_id;


    }

    public function view(): View
    {
        $elementos = App\elemento::
                        where("plantilla_id",$this->plantilla_id)
                        ->get();

        return view('excel.campos_plantilla.index', [
            'elementos' => $elementos
        ]);
    }
}
