<?php
    namespace App;

    use Illuminate\Database\Eloquent\Model;

    class emergencias extends Model
    {
        protected $primaryKey = "emergencias_id";
        protected $table = "emergencias";

        protected $fillable = ['razon_social','actividad','domicilio_legal','distrito','provincia',
        'subsector','persona1','persona2','correo1','correo2','telefono1','telefono2','nombre_instalacion','hora_inicio',
        'hora_termino','area_afectada','cantidad_derramada','unidad_derramada','emergencias_estado'];
    }