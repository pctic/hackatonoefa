<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class usuario_perfil extends Model
{

    use Notifiable;

    protected $primaryKey="usuario_id";
    protected $table="usuario_perfil";
}
