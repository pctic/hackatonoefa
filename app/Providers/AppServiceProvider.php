<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App;
use View;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */

    public function boot()
    {
        Schema::defaultStringLength(191);

        // $notificaciones = App\notificacion::where("perfil_id",1)
        //                         ->join("persona as p","p.usuario_id","notificacion.usuario_id")
        //                         ->join("codigo_notificacion as cn","cn.cn_id","notificacion.cn_id")
        //                         ->select(
        //                             "p.persona_nombre",
        //                             "notificacion.notificacion_id",
        //                             "notificacion.notificacion_mensaje",
        //                             "notificacion.perfil_id",
        //                             "notificacion.created_at",
        //                             "cn.cn_nombre",
        //                             "cn.cn_id"
        //                             )
        //                         ->where("notificacion.notificacion_estado",1)
        //                         ->get();

        // View::share('notificaciones', $notificaciones);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
