<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $primaryKey = 'usuario_id';
    protected $table = 'usuario';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'password', 'estado'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function persona()
    {
        return $this->hasOne('App\persona', 'usuario_id');
    }

    public static function persona_lista()
    {
        return persona::get();
    }

    public static function persona_datos($id = -1)
    {

        if( $id == "-1" ){

            return persona::where( "usuario_id" , session("usuario_id") )
                ->first();

        }

        else{

            return persona::where( "usuario_id" , $id )
                ->first();

        }

    }

}
