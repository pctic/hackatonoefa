<?php
    namespace App;

    use Illuminate\Database\Eloquent\Model;

    class registro_mc extends Model
    {
        protected $primaryKey = "registro_mc_id";
        protected $table = "registro_mc";

        protected $fillable = ['numero_mc','monto_sancion','resolucion','expediente','administrado','ubicacion','sector','materia','medidas_correctivas','fecha_limite','registro_mc_estado'];
    }